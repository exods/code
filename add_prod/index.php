<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

if (!$_GET['token'] == 'ssdfgsd2DGssd12') {
    die();
}
$APPLICATION->SetTitle("Добавление товаров Новость/Сет/Рецепт");
?>

<div class="container">
    <div class="row ">
        <div class="form_wrap">
            <form class="col-sm-9 ">
                <h1>Добавление товаров Новость/Сет/Рецепт</h1>
                <div class="form-group">
                    <label for="razdel">Раздел для добавления товаров</label>
                    <select required class="form-control" name="razdel">
                        <option value="40">Сеты</option>
                        <option value="21">Рецепты</option>
                        <option value="2">Новости</option>
                        <option value="49">Pinch</option>
                    </select>
                </div>
                <div class="form-group form_id">
                    <label for="ID">ID Элемента</label>
                    <input required type="text" name="ID" placeholder="ID элемента">
                </div>
                <div class="form-group">
                    <label for="article">Список артикулов через запятую</label>
                    <textarea required class="form-control" type="text" name="article" placeholder="Артикулы"></textarea>
                </div>
                <button type="submit"  class="btn btn-primary">Записать</button>
            </form>
            <div class="col-sm-9">
                <div class="result_products">
                </div>
            </div>
        </div>

    </div>
</div>
<style>
    .result_products {
        display: none;
    }

    .form-group {
        border: 1px solid grey;
        border-radius: 5px;
        padding: 10px;
    }

    .form_id {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
    .button {
        -webkit-transition: box-shadow 0.15s  ;
        -moz-transition: box-shadow 0.15s  ;
        -ms-transition: box-shadow 0.15s  ;
        -o-transition: box-shadow 0.15s  ;
        transition: box-shadow 0.15s  ;
        border: 1px solid #b2afa5;
        border-color: #e1e0dc #c9c8c1 #b2afa5;
        text-shadow: 0 1px 0 rgba(255, 255, 255, 0.439);
        background: -webkit-linear-gradient(top, #f6f5f0, #e1e0dc);
        background: -moz-linear-gradient(top, #f6f5f0, #e1e0dc);
        background: -ms-linear-gradient(top, #f6f5f0, #e1e0dc);
        background: -o-linear-gradient(top, #f6f5f0, #e1e0dc);
        background: linear-gradient(top, #f6f5f0, #e1e0dc);
        -pie-background: linear-gradient(top, #f6f5f0, #e1e0dc);
        color: #6b635b;
        -webkit-box-shadow: rgba(255, 255, 255, 0) 0 0 1px 0, rgba(255, 255, 255, 0) 0 0 0 1px inset, white 0 1px 0 0;
        -moz-box-shadow: rgba(255, 255, 255, 0) 0 0 1px 0, rgba(255, 255, 255, 0) 0 0 0 1px inset, white 0 1px 0 0;
        -ms-box-shadow: rgba(255, 255, 255, 0) 0 0 1px 0, rgba(255, 255, 255, 0) 0 0 0 1px inset, white 0 1px 0 0;
        -o-box-shadow: rgba(255, 255, 255, 0) 0 0 1px 0, rgba(255, 255, 255, 0) 0 0 0 1px inset, white 0 1px 0 0;
        box-shadow: rgba(255, 255, 255, 0) 0 0 1px 0, rgba(255, 255, 255, 0) 0 0 0 1px inset, white 0 1px 0 0;
        display: inline-block;
        white-space: nowrap;
        margin: 0;
        cursor: pointer;
        font-size: 20px;
        line-height: normal;
        text-align: center;
        color: #6b635b;
        padding: 30px 50px;
        text-decoration: none;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        -ms-border-radius: 3px;
        -o-border-radius: 3px;
        border-radius: 3px;
    }


    .button.disabled {
        color: #bdbbb9;
        border: 1px solid #d3d3d3;
        cursor: auto;
        text-shadow: white 0 1px 0;
        -webkit-box-shadow: white 0 1px 0 0 inset;
        -moz-box-shadow: white 0 1px 0 0 inset;
        -ms-box-shadow: white 0 1px 0 0 inset;
        -o-box-shadow: white 0 1px 0 0 inset;
        box-shadow: white 0 1px 0 0 inset;
    }


    .in-progress {
        text-shadow: none;
        background: -webkit-linear-gradient(-45deg, rgba(255, 255, 255, 0.6) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.6) 50%, rgba(255, 255, 255, 0.6) 75%, transparent 75%, transparent), -webkit-linear-gradient(top, #f6f5f0, #e1e0dc);
        background: -moz-linear-gradient(-45deg, rgba(255, 255, 255, 0.6) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.6) 50%, rgba(255, 255, 255, 0.6) 75%, transparent 75%, transparent), -moz-linear-gradient(top, #f6f5f0, #e1e0dc);
        background: -ms-linear-gradient(-45deg, rgba(255, 255, 255, 0.6) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.6) 50%, rgba(255, 255, 255, 0.6) 75%, transparent 75%, transparent), -ms-linear-gradient(top, #f6f5f0, #e1e0dc);
        background: -o-linear-gradient(-45deg, rgba(255, 255, 255, 0.6) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.6) 50%, rgba(255, 255, 255, 0.6) 75%, transparent 75%, transparent), -o-linear-gradient(top, #f6f5f0, #e1e0dc);
        background: linear-gradient(-45deg, rgba(255, 255, 255, 0.6) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.6) 50%, rgba(255, 255, 255, 0.6) 75%, transparent 75%, transparent), linear-gradient(top, #f6f5f0, #e1e0dc);
        -pie-background: linear-gradient(-45deg, rgba(255, 255, 255, 0.6) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, 0.6) 50%, rgba(255, 255, 255, 0.6) 75%, transparent 75%, transparent), linear-gradient(top, #f6f5f0, #e1e0dc);
        background-repeat: repeat;
        -webkit-background-size: 40px 40px, 100% 100%;
        -moz-background-size: 40px 40px, 100% 100%;
        -ms-background-size: 40px 40px, 100% 100%;
        -o-background-size: 40px 40px, 100% 100%;
        background-size: 40px 40px, 100% 100%;
        -webkit-animation: progress-bar-stripes 2s linear infinite;
        -moz-animation: progress-bar-stripes 2s linear infinite;
        -ms-animation: progress-bar-stripes 2s linear infinite;
        -o-animation: progress-bar-stripes 2s linear infinite;
        animation: progress-bar-stripes 2s linear infinite;
    }

    @-webkit-keyframes progress-bar-stripes {
        from { background-position: 0 0; }
        to { background-position: 40px 0; }
    }

    @-moz-keyframes progress-bar-stripes {
        from { background-position: 0 0; }
        to { background-position: 40px 0; }
    }

    @keyframes progress-bar-stripes {
        from { background-position: 0 0; }
        to { background-position: 40px 0; }
    }

</style>
<script>
    $(document).ready(function () {
        $('form').on('submit', function (e) {
            e.preventDefault()
            var $form = $(this);
            $form.find('button').addClass('in-progress').attr('disable','');
            var data = $form.serialize();
            var $results = $('.result_products');
            $results.slideUp();
            $.get("add_prod.php", data)
                .done(function (result) {
                    $form.find('button').removeClass('in-progress').removeAttr('disable');
                    $results.html(result);
                    $results.slideDown();
                });
        })
    })
</script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>

<?php

namespace App\Helpers;

use Bitrix\Main\Loader;
use Bitrix\Main\UserTable;
use CUser;
use RuntimeException;
use \Bitrix\Main\UserGroupTable;
use Bitrix\Socialnetwork\UserToGroupTable;

class UserHelper
{

    const MAX_SHOW_STAGE_IN_TASK_FILTER = 10;

	/**
	 * @param int $userId
	 *
	 * @return int
	 */
	public static function getCompanyId(int $userId = 0): int
	{
		if ($userId == 0) {
			if ($GLOBALS['USER'] instanceof CUser && $GLOBALS['USER']->isAuthorized()) {
				$userId = $GLOBALS['USER']->GetID();
			} else {
				throw new RuntimeException("User not authorized. User's company not defined.");
			}
		}

		$userIterator = UserTable::getList([
			'filter' => ['ID' => $userId],
			'select' => ['ID', 'UF_COMPANY'],
		]);

		if ($user = $userIterator->fetch() ) {

			return $user['UF_COMPANY'] ?: 0;
		}

		return 0;
	}

	/**
	 * @param int $companyId
	 *
	 * @return array

	 */
	public static function getUsersIdsByCompanyId(int $companyId): array
	{
		$usersIds = [];

		$result = UserTable::getList([
			'filter' => ['UF_COMPANY' => $companyId],
			'select' => ['ID']
		])->fetchAll();

		return array_column($result, 'ID');
	}

    /**
     * @param int $userId
     * @param bool $noSN
     * @return string
     */
    public static function getFullName(int $userId, bool $noSN = false): string
    {
        $arSelect = ['LAST_NAME','NAME'];
        if (!$noSN)
        {
            $arSelect[] = 'SECOND_NAME';
        }
        $result = UserTable::getList([
            'filter' => ['ID' => $userId],
            'select' => $arSelect
        ])->fetch();
        if ($result)
        {
            return implode(' ',$result);
        }
        else
        {
            return 'Сотрудник не найден';
        }
    }

    /**
     * @param array $arGroups
     * @return array
     */
    public static function getGroupUsers(array $arGroups): array
    {
        $arResult = [];
        $result = UserGroupTable::getList(array(
            'filter' => array('GROUP.ACTIVE'=>'Y', 'GROUP_ID' => $arGroups, 'USER.ACTIVE'=>'Y'),
            'select' => array('USER_ID'),
            'order' => array('USER.ID'=>'DESC'),
        ));
        while ($arGroup = $result->fetch()) {
            $arResult[] = $arGroup['USER_ID'];
        }

        return $arResult;
    }

    /**
     * Выборка проектов пользователя
     *
     * @param int $userId
     * @return array
     */
    public static function getSocialGroupUsers(int $userId)
    {
        Loader::includeModule('socialnetwork');
        $rsGroups = UserToGroupTable::getList([
            'select' => ['GROUP_ID'],
            'filter' => [
                'USER_ID' => $userId,
                '=ROLE' => [SONET_ROLES_OWNER, SONET_ROLES_MODERATOR, SONET_ROLES_USER]
            ],
            'group' => ['GROUP_ID']
        ]);
        $arResult = [];
        while ($arGroup = $rsGroups->Fetch()) {
            $arResult[] = $arGroup['GROUP_ID'];
        }
        $arResult = array_values(array_unique($arResult));

        return $arResult;
    }

    /**
     * Выборка проектов пользователя на основе задач
     *
     * @param int $userId
     * @return array
     */
    public static function getSocialGroupUsersByTasks(int $userId)
    {
        Loader::includeModule('tasks');
        $res = \CTasks::GetList(
            ['ID' => 'ASC'],
            [
                'CHECK_PERMISSIONS' => 'N',
                '!GROUP_ID' => false,
                '::SUBFILTER-1' => array(
                    '::LOGIC' => 'OR',
                    '::SUBFILTER-1' => array(
                        'CREATED_BY' => $userId,
                    ),
                    '::SUBFILTER-2' => array(
                        'RESPONSIBLE_ID' => $userId,
                    ),
                    '::SUBFILTER-3' => array(
                        'ACCOMPLICE' => $userId,
                    ),
                    '::SUBFILTER-4' => array(
                        'AUDITOR' => $userId,
                    ),
                ),
            ],
            ['GROUP_ID']
        );
        $arResult = [];
        while ($arTask = $res->Fetch()) {
            if ($arTask['GROUP_ID']) {
                $arResult[] = $arTask['GROUP_ID'];
            }
        }
        $arResult = array_values(array_unique($arResult));

        return $arResult;
    }

}
import './modules/btn-effect.js';

import './modules/sliders/main-sale-slider';

import './modules/sliders/mainscreen-slider';

import './modules/sliders/main-community-slider';

import './modules/sliders/main-news-slider';

import './modules/sliders/compare-slider';

import './modules/sliders/analogs-popups-slider';

import './modules/sliders/catalog-main-slider';

import './modules/tabs';

import './modules/spollers';

import './modules/header-drop-menu';

import './modules/slim-select';

import './modules/catalog-drop-list';

// import './modules/calc-product';

import './modules/submenu-header.js';

import './modules/header-scroll';

import './modules/form-validator';

import './modules/phone-mask/phone-mask.js';

import './modules/popups/popups';

import './modules/card-product-like';

import './modules/burger';

import './modules/tippy';

import './modules/dynamic-adapt';

import './modules/first-letter';

import './modules/search-header';

import './modules/sliders/description-product-slider';

import './modules/product-switch-title';

import './modules/sliders/examples-product-slider';

import './modules/sliders/product-slider';

import './modules/sliders/analog-slider';

import './modules/file-uploader';

import './modules/file-doc-uploader';

import './modules/btn-save-changes';

import './modules/custom-input-range';

import './modules/cookie';

import './modules/favorites-projects-delete';

import './modules/catalog-switch-cards';

import './modules/favorites-specifications';

import './modules/glightbox';

import './modules/videos-lazy';

import './modules/360-view';

import './modules/off-animations';

import './modules/lk-managers';

import './modules/location';

import './modules/popups/popup-leftovers';

import Swiper from 'swiper';

// const descriptionProductThumbs = document.querySelector(
//   '.thumbs-description-product__container'
// );
// const myDescriptionProductThumbs = new Swiper(descriptionProductThumbs, {
//   speed: 800,
//   direction: 'vertical',
//   loop: true,
//   freeMode: true,
//   slidesPerView: 3,
//   centeredSlides: true,
//   spaceBetween: 5,
//   slideClass: 'thumb-description-product',
//   navigation: {
//     nextEl: '.thumbs-description-product__arrow--next',
//     prevEl: '.thumbs-description-product__arrow--prev',
//   },
// });

// const descriptionProductSlider = document.querySelector(
//   '.slider-description-product__container'
// );

// new Swiper(descriptionProductSlider, {
//   // slidesPerView: 1,
//   // followFinger: false,
//   // simulateTouch: false,
//   speed: 800,
//   loop: true,
//   slideClass: 'slide-description-product',
//   thumbs: {
//     swiper: myDescriptionProductThumbs,
//   },
//   navigation: {
//     nextEl: myDescriptionProductThumbs.navigation.nextEl,
//     prevEl: myDescriptionProductThumbs.navigation.prevEl,
//   },
// });

const descriptionProductSlider = document.querySelectorAll(
  '.slider-description-product__container'
);

function initMainSlider(slider) {
  const tumbs = slider.parentNode.querySelector(
    '.thumbs-description-product__container'
  );
  const mySwiperNav = new Swiper(tumbs, {
    slidesPerView: 'auto',
    loop: false,
    centeredSlides: true,
    spaceBetween: 5,
    direction: 'horizontal',
    freeMode: true,
    navigation: {
      nextEl: '.thumbs-description-product__arrow--next',
      prevEl: '.thumbs-description-product__arrow--prev',
    },
    breakpoints: {
      769: {
        // slidesPerView: 3,
        direction: 'vertical',
      },
      320: {
        direction: 'horizontal',
      },
    },
  });
  new Swiper(slider, {
    spaceBetween: 30,
    loop: false,
    thumbs: {
      swiper: mySwiperNav,
    },
    navigation: {
      nextEl: mySwiperNav.navigation.nextEl,
      prevEl: mySwiperNav.navigation.prevEl,
    },
  });
  const slideProduct = document.querySelectorAll('.slide-description-product');
  if (slideProduct.length === 1) {
    tumbs.style.display = 'none';
  }
}

descriptionProductSlider.forEach((slider) => {
  initMainSlider(slider);
});

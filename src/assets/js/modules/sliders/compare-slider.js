/* eslint-disable */
import Swiper from 'swiper';

let goodsPropertyDesktop = [];
let goodsPropertyMobile = [];
let goodsSliders = [];

function initComparsion() {
	let comparsionGoodsSlider = document.querySelectorAll('.comparsion-goods-slider');
	let comparsionPropertyDesktop = document.querySelectorAll('.comparsion-property-slider--desktop');
	let comparsionPropertyMobile = document.querySelectorAll('.comparsion-property-slider--mobile');

	comparsionPropertyDesktop.forEach((slider) => {
		var mySwiper = new Swiper(slider, {

			freeMode: false,

			spaceBetween: 20,
			
			// navigation: {
			// 	prevEl: '.arrow-prev',
			// 	nextEl: '.arrow-next',
			// },

			speed: 500,
			breakpoints: {
				1460: {
					slidesPerView: 3,
				},
				1180: {
					slidesPerView: 2,
				},
				325: {
					slidesPerView: 1,
					spaceBetween: 10,
				}
			},
		});

		mySwiper.init();

		goodsPropertyDesktop.push(mySwiper);

		goodsSliders.push(mySwiper)
	})

	comparsionPropertyMobile.forEach((slider) => {
		var mySwiper = new Swiper(slider, {

			speed: 500,

			spaceBetween: 20,

			breakpoints: {
				1460: {
					slidesPerView: 3,
				},
				1180: {
					slidesPerView: 2,
				},
				325: {
					slidesPerView: 1,
					spaceBetween: 10,
				}
			},
			init: false
		});

		mySwiper.init();

		goodsPropertyMobile.push(mySwiper);

		goodsSliders.push(mySwiper)
	})

	comparsionGoodsSlider.forEach((slider, index) => {
		var mySwiper = new Swiper(slider, {
			speed: 500,
			spaceBetween: 20,
			breakpoints: {
				1460: {
					slidesPerView: 3,
				},
				1180: {
					slidesPerView: 2,
				},
				325: {
					slidesPerView: 1,
					spaceBetween: 10,
				}
			},

			controller: {
				control: index === 0 ? goodsPropertyDesktop : goodsPropertyMobile,
			},

			init: false
		});
		
		// let buttonsComparsion = document.querySelectorAll('.button-comparsion')[0];
		let buttonsContainer = mySwiper.el.closest('.goods-comparsion__desktop')

		if(buttonsContainer) {
			let leftButton = buttonsContainer.querySelectorAll('.arrow-prev')[0];
			let rightButton = buttonsContainer.querySelectorAll('.arrow-next')[0];
			mySwiper.on('slideChange init', function () {
				if (mySwiper.isBeginning) {
					 leftButton.classList.add('swiper-button-disabled');
				} else {
					leftButton.classList.remove('swiper-button-disabled');
				}
				if (mySwiper.isEnd) {
					rightButton.classList.add('swiper-button-disabled');
				} else {
					rightButton.classList.remove('swiper-button-disabled');
				}
			})
			leftButton.addEventListener('click', function () {
				mySwiper.slidePrev();
			})
	
			rightButton.addEventListener('click', function () {
				mySwiper.slideNext();
			})
		}
		
		mySwiper.init();

		goodsSliders.push(mySwiper)

		if (index === 0) {
			goodsPropertyDesktop.forEach(item => {
				item.controller.control = mySwiper
			})
		} else {
			goodsPropertyMobile.forEach(item => {
				item.controller.control = mySwiper
			})
		}
	})
}

function destroyComparison() {
	goodsSliders.forEach((item) => {
		item.destroy();
	})
	goodsSliders = [];
	goodsPropertyDesktop = [];
	goodsPropertyMobile = [];
}

window.initComparsion = initComparsion;
window.destroyComparison = destroyComparison;

initComparsion();
/* eslint-enable */

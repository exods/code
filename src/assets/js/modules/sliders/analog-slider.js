import Swiper from 'swiper';

function initAnalogSlider() {
  const sliderAnalog = document.querySelectorAll('.analogs-slider__container');
  sliderAnalog.forEach((el) => {
    new Swiper(el, {
      slideClass: 'analog-slide',
      speed: 800,
      observeParents: true,
      observer: true,
      observeSlideChildren: true,
      autoplay: {
        delay: 5000,
        disableOnInteraction: false,
      },
      navigation: {
        prevEl: '.analogs-slider__container .arrow-prev',
        nextEl: '.analogs-slider__container .arrow-next',
      },
      breakpoints: {
        0: {
          spaceBetween: 25,
          slidesPerView: 1,
        },
        480: {
          spaceBetween: 10,
          slidesPerView: 1,
        },
        769: {
          slidesPerView: 2,
          spaceBetween: 15,
        },
        1025: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        1201: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
      },
    });
  });
}

initAnalogSlider();
window.initAnalogSlider = initAnalogSlider;

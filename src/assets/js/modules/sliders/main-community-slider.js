import Swiper from 'swiper';

const sliderMainCommunity = document.querySelectorAll(
  '.slider-main-community__container'
);
if (sliderMainCommunity) {
  sliderMainCommunity.forEach((el) => {
    new Swiper(el, {
      slideClass: 'slide-main-community',
      speed: 800,
      navigation: {
        prevEl: '.slider-main-community__container .arrow-prev',
        nextEl: '.slider-main-community__container .arrow-next',
      },
      breakpoints: {
        0: {
          spaceBetween: 15,
          slidesPerView: 2,
        },
        650: {
          spaceBetween: 15,
          slidesPerView: 3,
        },
        900: {
          spaceBetween: 20,
          slidesPerView: 4,
        },
      },
    });
  });
}

// new Swiper('.slider-main-community__container', {
//   slideClass: 'slide-main-community',
//   speed: 800,
//   spaceBetween: 25,
//   loop: true,
//   navigation: {
//     nextEl: '.slider-main-community__container .arrow-next',
//   },
//   breakpoints: {
//     0: {
//       spaceBetween: 15,
//       slidesPerView: 2,
//     },
//     650: {
//       slidesPerView: 3,
//     },
//     900: {
//       spaceBetween: 20,
//       slidesPerView: 4,
//     },
//   },
// });

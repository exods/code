import Swiper from 'swiper';

const sliderMainSale = document.querySelectorAll(
  '.slider-main-sale__container'
);
if (sliderMainSale) {
  sliderMainSale.forEach((el) => {
    new Swiper(el, {
      slideClass: 'slide-main-sale',
      speed: 800,
      loop: false,
      autoplay: {
        delay: 5000,
        disableOnInteraction: true,
      },
      navigation: {
        prevEl: '.slider-main-sale__container .arrow-prev',
        nextEl: '.slider-main-sale__container .arrow-next',
      },
      breakpoints: {
        0: {
          spaceBetween: 15,
          slidesPerView: 1,
        },
        650: {
          slidesPerView: 2,
          spaceBetween: 15,
        },
        900: {
          spaceBetween: 25,
          slidesPerView: 3,
        },
      },
    });
  });
}

// new Swiper('.slider-main-sale__container', {
//   slideClass: 'slide-main-sale',
//   speed: 800,
//   spaceBetween: 25,
//   loop: true,
//   navigation: {
//     nextEl: '.slider-main-sale .arrow-next',
//   },
//   breakpoints: {
//     0: {
//       spaceBetween: 15,
//       slidesPerView: 1,
//     },
//     650: {
//       spaceBetween: 15,
//       slidesPerView: 2,
//     },
//     900: {
//       spaceBetween: 25,
//       slidesPerView: 3,
//     },
//   },
// });

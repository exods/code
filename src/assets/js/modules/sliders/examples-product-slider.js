import Swiper from 'swiper';

function initExamplesProductSlider() {
  const examplesProductSlider = document.querySelectorAll('.js-slider-e-p');
  function initExamplesSlider(slider) {
    const thumbs = slider.parentNode.querySelector('.js-t-e-p');
    const mySwiperNav = new Swiper(thumbs, {
      slidesPerView: 'auto',
      spaceBetween: 10,
      direction: 'horizontal',
      freeMode: true,
      slideClass: 'thumb-examples-product',
      observer: true,
      breakpoints: {
        600: {
          spaceBetween: 20,
          direction: 'vertical',
        },
        480: {
          spaceBetween: 10,
          direction: 'vertical',
        },
        320: {
          spaceBetween: 10,
          direction: 'horizontal',
        },
      },
    });
    new Swiper(slider, {
      spaceBetween: 20,
      slideClass: 'slide-examples-product',
      thumbs: {
        swiper: mySwiperNav,
      },
    });
  }
  examplesProductSlider.forEach((slider) => {
    initExamplesSlider(slider);
  });
}

initExamplesProductSlider();

window.initExamplesProductSlider = initExamplesProductSlider;

import Swiper from 'swiper';

function initProductSliderNew() {
  const sliderMainNews = document.querySelectorAll(
    '.product-slider__container'
  );
  sliderMainNews.forEach((el) => {
    const instance = new Swiper(el, {
      slideClass: 'product-slide',
      speed: 800,
      autoplay: {
        delay: 5000,
        disableOnInteraction: false,
      },
      navigation: {
        prevEl: '.product-slider__container .arrow-prev',
        nextEl: '.product-slider__container .arrow-next',
      },
      breakpoints: {
        0: {
          spaceBetween: 25,
          slidesPerView: 1,
        },
        480: {
          spaceBetween: 15,
          slidesPerView: 2,
        },
        769: {
          slidesPerView: 3,
          spaceBetween: 10,
        },
        1025: {
          slidesPerView: 3,
          spaceBetween: 20,
        },
        1201: {
          slidesPerView: 4,
          spaceBetween: 20,
        },
      },
    });
    el.addEventListener('mouseenter', () => {
        instance.autoplay.stop();
    });
    el.addEventListener('mouseleave', () => {
        instance.autoplay.run();
    });
  });
}

initProductSliderNew();
window.initProductSliderNew = initProductSliderNew;

import Swiper from 'swiper';

const sliderMainScreen = document.querySelectorAll(
  '.slider-mainscreen__container'
);
if (sliderMainScreen) {
  sliderMainScreen.forEach((el) => {
    new Swiper(el, {
      slideClass: 'slide-mainscreen',
      speed: 800,
      spaceBetween: 15,
      autoplay: {
        delay: 5000,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.slider-mainscreen__pagging',
        clickable: true,
        type: 'bullets',
      },
      navigation: {
        nextEl: '.slider-mainscreen__container .arrow-next',
        prevEl: '.slider-mainscreen__container .arrow-prev',
      },
    });
  });
}

const mainScreenSlides = document.querySelectorAll(
  '.slider-mainscreen__container .swiper-wrapper .slide-mainscreen'
);
const mainScreenPagination = document.querySelector(
  '.slider-mainscreen__pagging'
);

for (let i = 0; i < mainScreenSlides.length; i++) {
  if (mainScreenSlides.length === 1) {
    mainScreenPagination.classList.add('disabled');
  }
}

// new Swiper('.slider-mainscreen__container', {
//   slideClass: 'slide-mainscreen',
//   speed: 800,
//   spaceBetween: 15,
//   pagination: {
//     el: '.slider-mainscreen__pagging',
//     clickable: true,
//     type: 'bullets',
//   },
//   navigation: {
//     nextEl: '.mainscreen .arrow-next',
//     prevEl: '.mainscreen .arrow-prev',
//   },
// });

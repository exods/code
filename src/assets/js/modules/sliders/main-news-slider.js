import Swiper from 'swiper';

const sliderMainNews = document.querySelectorAll(
  '.slider-main-news__container'
);
if (sliderMainNews) {
  sliderMainNews.forEach((el) => {
    new Swiper(el, {
      slideClass: 'slide-main-news',
      speed: 800,
      loop: true,
      navigation: {
        prevEl: '.slider-main-news__container .arrow-prev',
        nextEl: '.slider-main-news__container .arrow-next',
      },
      breakpoints: {
        0: {
          spaceBetween: 10,
          slidesPerView: 1,
        },
        480: {
          spaceBetween: 10,
          slidesPerView: 2,
        },
        769: {
          spaceBetween: 10,
          slidesPerView: 3,
        },
        900: {
          spaceBetween: 20,
          slidesPerView: 3,
        },
      },
    });
  });
}

// new Swiper('.slider-main-news__container', {
//   slideClass: 'slide-main-news',
//   slidesPerView: 3,
//   speed: 800,
//   spaceBetween: 25,
//   loop: true,
//   navigation: {
//     nextEl: '.slider-main-news .arrow-next',
//   },
//   breakpoints: {
//     0: {
//       spaceBetween: 15,
//       slidesPerView: 1,
//     },
//     600: {
//       slidesPerView: 2,
//     },
//     900: {
//       spaceBetween: 20,
//       slidesPerView: 3,
//     },
//   },
// });

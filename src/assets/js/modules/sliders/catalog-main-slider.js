import Swiper from 'swiper';
import { fadeOut, fadeIn } from '../libs/fade';

const clsWrapSlider = '.js-catalog-slider-mainscreen';

const sliderMainScreen = document.querySelectorAll(clsWrapSlider);
if (sliderMainScreen) {
  sliderMainScreen.forEach((el) => {
    new Swiper(el, {
      slideClass: 'slider-item',
      speed: 800,
      spaceBetween: 15,
      autoHeight: true,
      autoplay: {
        delay: 5000,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.slider-mainscreen__pagging',
        clickable: true,
        type: 'bullets',
      },
      navigation: {
        nextEl: `${clsWrapSlider} .arrow-next`,
        prevEl: `${clsWrapSlider} .arrow-prev`,
      },
    });
  });
}

const mainScreenSlides = document.querySelectorAll(
  `${clsWrapSlider} .swiper-wrapper .slider-item`
);
const mainScreenPagination = document.querySelector(
  '.slider-mainscreen__pagging'
);

for (let i = 0; i < mainScreenSlides.length; i++) {
  if (mainScreenSlides.length === 1) {
    mainScreenPagination.classList.add('disabled');
  }
}

function initExamplesProductSlider() {
  const examplesProductSlider = document.querySelectorAll(
    '.js-slider-c-t-container'
  );
  function initExamplesSlider(slider) {
    const thumbs = slider.parentNode.querySelector('.js-thumbs-c-t-container');
    // eslint-disable-next-line no-unused-vars
    const mySwiperNav = new Swiper(thumbs, {
      slidesPerView: 'auto',
      spaceBetween: 10,
      direction: 'horizontal',
      freeMode: true,
      slideClass: 'js-t-e-p-video',
      observer: true,
      navigation: {
        nextEl: '.js-t-c-p-next',
        prevEl: '.js-t-c-p-prev',
      },
      breakpoints: {
        600: {
          spaceBetween: 20,
          direction: 'vertical',
        },
        480: {
          spaceBetween: 10,
          direction: 'vertical',
        },
        320: {
          spaceBetween: 10,
          direction: 'horizontal',
        },
      },
    });
    new Swiper(slider, {
      spaceBetween: 20,
      slideClass: 'slide-examples-product',
      autoHeight: true,
      thumbs: {
        swiper: mySwiperNav,
      },
      navigation: {
        nextEl: mySwiperNav.navigation.nextEl,
        prevEl: mySwiperNav.navigation.prevEl,
      },
    });
  }
  examplesProductSlider.forEach((slider) => {
    initExamplesSlider(slider);
  });
}

initExamplesProductSlider();

/* global YT */

const video = document.querySelector('.js-s-e-p-video');
if (video) {
  const tag = document.createElement('script');
  tag.src = 'https://www.youtube.com/iframe_api';
  const firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  [...document.querySelectorAll('.js-s-e-p-btn-play')].forEach((btnPlay) => {
    let player = null;
    let startPlay = false;
    let playerReady = false;
    let pauseTimeout = null;
    const classIframe = '.js-s-e-p-video-iframe';

    let playerInit = false;
    btnPlay.addEventListener('click', function () {
      if (!playerInit) {
        // можно добавить прелоадер
        const self = this;
        const wrapper = this.closest('.js-s-e-p-video');
        const iframe = wrapper.querySelector(classIframe);
        const picture = wrapper.querySelector('.js-s-e-p-picture');
        const url = wrapper.getAttribute('data-src');

        player = new YT.Player(iframe, {
          width: '100%',
          height: '100%',
          videoId: url,
          playerVars: {
            constrols: 0,
            showinfo: 0,
            enablejsapi: 1,
            modestbranding: 1,
            rel: 0,
          },
          events: {
            onReady(event) {
              playerReady = true;
              // Видео загрузилось, теперь можно и показывать.
              if (startPlay) {
                const iframeNew = wrapper.querySelector(classIframe);
                fadeOut(self);
                fadeOut(picture);
                fadeIn(iframeNew);
                iframeNew.classList.add('is-show');
                event.target.playVideo();
              }
            },
            onStateChange(event) {
              const iframeNew = wrapper.querySelector(classIframe);
              clearTimeout(pauseTimeout);
              if (event.data === YT.PlayerState.ENDED) {
                player.stopVideo();
                fadeOut(iframeNew);
                fadeIn(picture);
                fadeIn(self);
                iframeNew.classList.remove('is-show');
              }
              if (event.data === YT.PlayerState.PAUSED) {
                player.pauseVideo();
                pauseTimeout = setTimeout(() => {
                  fadeOut(iframeNew);
                  fadeIn(picture);
                  fadeIn(self);
                  iframeNew.classList.remove('is-show');
                }, 10000);
              }
              if (event.data === YT.PlayerState.PLAYING) {
                fadeOut(self);
                fadeOut(picture);
                if (!iframeNew.classList.contains('is-show')) {
                  fadeIn(iframeNew);
                }
              }
            },
          },
        });
        playerInit = true;
        [...document.querySelectorAll('.js-t-e-p-video')].forEach((thumb) => {
          thumb.addEventListener('click', () => {
            if (player) {
              player.pauseVideo();
            }
          });
        });
      }

      startPlay = true;
      if (playerReady) {
        player.playVideo();
      }
    });
  });
}

window.initExamplesProductSlider = initExamplesProductSlider;

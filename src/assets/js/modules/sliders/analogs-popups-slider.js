import Swiper from 'swiper';

const analogsPopupSliders = document.querySelectorAll('.analogs__slider');

analogsPopupSliders.forEach((el) => {
  const analogPopupSlider = el.querySelector('.analogs__slider-swiper');
  new Swiper(analogPopupSlider, {
    speed: 1000,
    observeParents: true,
    observer: true,
    observeSlideChildren: true,
    slideToClickedSlide: true,
    navigation: {
      prevEl: el.querySelector('.swiper-button-prev'),
      nextEl: el.querySelector('.swiper-button-next'),
    },
    breakpoints: {
      0: {
        slidesPerView: 2,
        spaceBetween: 20,
      },
      460: {
        slidesPerView: 'auto',
        spaceBetween: 20,
      },
    },
  });
});

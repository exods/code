import initEffectButton from './function-init-effect-button';

const buttons = document.querySelectorAll('._btn');

buttons.forEach((button) => {
  initEffectButton(button);
});

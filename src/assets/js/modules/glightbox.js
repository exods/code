import GLightbox from 'glightbox';

/* eslint-disable */
function initLightBox(){
  const lightbox = GLightbox({
    selector: '.glightbox',
    touchNavigation: true,
    loop: true,
    autoplayVideos: true,
    plyr: {
      config: {
        ratio: '16:9', // or '4:3'
        muted: false,
        hideControls: true,
        youtube: {
          noCookie: true,
          rel: 0,
          showinfo: 0,
          iv_load_policy: 3,
        },
        vimeo: {
          byline: false,
          portrait: false,
          title: false,
          speed: true,
          transparent: false,
        },
      },
    },
  });
}
initLightBox();
window.initLightBox = initLightBox;
/* eslint-enable */

class FilesDocUploader {
  constructor(filesDocContainer) {
    this.filesDocContainer = filesDocContainer;
    this.previews = filesDocContainer.querySelector(
      '.files-doc-uploader__previews'
    );
    this.input = filesDocContainer.querySelector('input');
    this.files = [];

    this.init();
  }

  getDocFilelist() {
    const docFileList =
      new ClipboardEvent('').clipboardData || new DataTransfer();

    this.files.forEach((file) => {
      docFileList.items.add(file);
    });

    return docFileList.files;
  }

  addDocEvents() {
    this.input.addEventListener('change', () => {
      this.files.push(...this.input.files);
      this.updateDocPreview();
      this.updateDocInputValue();
    });
  }

  updateDocInputValue() {
    this.input.value = '';
    this.input.files = this.getDocFilelist();
    this.input.customFiles = this.files;
  }

  updateDocPreview() {
    this.previews.innerHTML = '';

    this.files.forEach((file) => {
      this.previews.insertAdjacentHTML(
        'beforeend',
        `
              <div class="files-doc-uploader__preview">
                  <div class="files-doc-uploader__preview-close">
                    <svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M10.2734 9.12822L9.13371 10.268L5.13905 6.27468L1.14219 10.2719L0.00127276 9.13096L3.99793 5.13396L0.00157065 1.13899L1.1413 -0.000732372L5.1376 3.99418L9.13142 1.2038e-05L10.2723 1.14093L6.27872 5.1349L10.2734 9.12822Z" fill="white"/>
                    </svg>                
                  </div>
                  <div class="files-doc-uploader__preview-icon">
                    <svg width="18" height="22" viewBox="0 0 18 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd" clip-rule="evenodd" d="M2 22C0.89543 22 0 21.1046 0 20V2C0 0.89543 0.895431 0 2 0H13.5L18 4.82609V20C18 21.1046 17.1046 22 16 22H2ZM16 20V5.61386L12.6303 2H2V20H16ZM5 10C5 9.44771 5.44772 9 6 9H12C12.5523 9 13 9.44771 13 10C13 10.5523 12.5523 11 12 11H6C5.44772 11 5 10.5523 5 10ZM6 13C5.44772 13 5 13.4477 5 14C5 14.5523 5.44772 15 6 15H10C10.5523 15 11 14.5523 11 14C11 13.4477 10.5523 13 10 13H6Z" fill="#474747"/>
                    </svg>                  
                  </div>
                  <div class="files-doc-uploader__preview-name">
                    ${file.name}
                  </div>
              </div>
            `
      );
    });

    const previewsDoc = this.previews.querySelectorAll(
      '.files-doc-uploader__preview'
    );
    previewsDoc.forEach((preview, index) => {
      this.addEventRemoveDocFile(preview, index);
    });
  }

  removeDocFile(index) {
    this.files.splice(index, 1);
    this.updateDocInputValue();
    this.updateDocPreview();
  }

  addEventRemoveDocFile(preview, index) {
    const closeDocButton = preview.querySelector(
      '.files-doc-uploader__preview-close'
    );

    closeDocButton.addEventListener('click', () => {
      this.removeDocFile(index);
    });
  }

  init() {
    this.addDocEvents();
  }
}

function reInitDocUpd() {
  const filesDocUploaders = document.querySelectorAll('.files-doc-uploader');
  filesDocUploaders.forEach((reFilesDocUploader) => {
    new FilesDocUploader(reFilesDocUploader);
  });
}

reInitDocUpd();

window.reInitDocUpd = reInitDocUpd;

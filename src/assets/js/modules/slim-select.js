import SlimSelect from 'slim-select';

function initSlimSelect() {
  const customSelects = document.querySelectorAll('.custom-select');
  customSelects.forEach((selectElement) => {
    if (!!selectElement.dataset.ssid || selectElement.tagName !== 'SELECT') {
      return;
    }
    new SlimSelect({
      select: selectElement,
      showSearch: false,
      placeholder: 'Выбрать из списка',
      allowDeselect: true,
    });
  });

  const customSelectsLocation = document.querySelectorAll(
    '.custom-select-location'
  );
  customSelectsLocation.forEach((selectElement) => {
    if (!!selectElement.dataset.ssid || selectElement.tagName !== 'SELECT') {
      return;
    }
    new SlimSelect({
      select: selectElement,
      showSearch: true,
      allowDeselect: true,
      placeholder: 'Выбрать город',
      searchPlaceholder: 'Найти город',
    });
  });

  const customSelectsNoDelete = document.querySelectorAll(
    '.custom-select--no-delete'
  );
  customSelectsNoDelete.forEach((selectElement) => {
    if (!!selectElement.dataset.ssid || selectElement.tagName !== 'SELECT') {
      return;
    }
    new SlimSelect({
      select: selectElement,
      showSearch: false,
    });
  });

  const catalogSelects = document.querySelectorAll('.custom-select--catalog');
  catalogSelects.forEach((selectElement) => {
    if (!!selectElement.dataset.ssid || selectElement.tagName !== 'SELECT') {
      return;
    }
    new SlimSelect({
      select: selectElement,
      showSearch: false,
      placeholder: 'Сортировать по:',
      allowDeselect: true,
    });
  });
}

initSlimSelect();
window.initSlimSelect = initSlimSelect;

function customSelect() {
  const customSelectBlock = document.querySelector('.custom-select');
  if (customSelectBlock) {
    initSlimSelect();
  }
}

window.customSelect = customSelect;

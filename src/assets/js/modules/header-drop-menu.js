const clickOutside = require('click-outside');

const dropMenuButtons = document.querySelectorAll('.drop-menu');
dropMenuButtons.forEach((dropMenuButton) => {
  dropMenuButton.addEventListener('click', () => {
    dropMenuButton.parentNode.classList.toggle('drop-menu-active');
  });
  clickOutside(dropMenuButton.parentNode, () => {
    dropMenuButton.parentNode.classList.remove('drop-menu-active');
  });
});

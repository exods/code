const catalogSwitchGrid = document.querySelector('.switch-catalog__item--grid');
const catalogSwitchList = document.querySelector('.switch-catalog__item--list');
const catalogBody = document.querySelector('.catalog-lvl-three__body');

function cardChange() {
  const productCards = document.querySelectorAll('.product-card');

  productCards.forEach((card) => {
    card.classList.add('product-card--row');
    if (catalogBody.classList.contains('catalog-lvl-three__body--grid')) {
      card.classList.remove('product-card--row');
    }
  });
}
if (catalogSwitchList) {
  catalogSwitchList.addEventListener('click', () => {
    if (catalogBody.classList.contains('catalog-lvl-three__body--grid')) {
      catalogBody.classList.remove('catalog-lvl-three__body--grid');
      cardChange();
    }
  });
}

if (catalogSwitchGrid) {
  catalogSwitchGrid.addEventListener('click', () => {
    catalogBody.classList.add('catalog-lvl-three__body--grid');
    cardChange();
  });
}

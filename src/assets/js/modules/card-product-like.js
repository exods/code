const likesCardProduct = document.querySelectorAll('.product-card__icon');

likesCardProduct.forEach((like) => {
  like.addEventListener('click', () => {
    like.classList.toggle('product-card__icon--action');
  });
});

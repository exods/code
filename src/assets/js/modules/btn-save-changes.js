const lkForm = document.querySelector('.profile-form');

if (lkForm) {
  const btnSaveChangesShow = document.querySelector('.save-change');

  if (btnSaveChangesShow) {
    lkForm.addEventListener('input', () => {
      btnSaveChangesShow.classList.add('show');
    });
  }
}

import axios from 'axios';

const getElement = (className) => [...document.querySelectorAll(className)];

const showNotInfo = () => {
  getElement('.js-leftovers-not-info').forEach((el) =>
    el.classList.add('active')
  );
};

let loadData = false;

getElement('.js-availability-in-warehouses').forEach((element) => {
  element.addEventListener('click', async (event) => {
    if (loadData) {
      return false;
    }

    try {
      const url = event.currentTarget.dataset.url;
      const data = await axios.get(url);

      loadData = true;

      if (data.length === 0) {
        showNotInfo();
        return false;
      }

      // prettier-ignore
      const bodyContent = data.reduce((sum, item) => (
          `${sum}<tr>` +
            `<td>${item.city}</td>` +
            `<td>${item.regional ? item.regional : '—'}</td>` +
            `<td>Штука/метр</td>` +
            `<td>${item.central ? item.central : '—'}</td>` +
          `</tr>`
      ), '');

      // prettier-ignore
      const table =  '<table class="leftovers-table">' +
                  '<thead>'+
                    '<tr>'+
                      '<th colspan="3">Региональный склад</th>'+
                      '<th>Центральный склад</th>'+
                    '</tr>'+
                    '<tr>'+
                      '<th>Склад</th>'+
                      '<th>Наличие</th>'+
                      '<th>ЕИ</th>'+
                      '<th>Наличие</th>'+
                    '</tr>'+
                  '</thead>'+
                  `<tbody>${bodyContent}</tbody>` +
                '</table>';
      /* Такой синтаксис, чтоб не было большого количества пробелов в min.js файле */

      getElement('.js-wrapper-leftovers').forEach((el) => {
        el.innerHTML = table;
      });
    } catch (e) {
      showNotInfo();
      console.log('e:', e); // eslint-disable-line
    }
    return false;
  });
});

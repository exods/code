/* eslint-disable */
const body = document.querySelector('body');
const lockPadding = document.querySelectorAll(".lock-padding");

let unlock = true;

const timeout = 800;
const timeToLoadNodes = 200;

window.popupsInit = function () {

	const moduleName = 'popups';

	Array.from(document.querySelectorAll('.popup')).forEach((self) => {
		if (self.dataset[moduleName + 'Init']) {
			return;
		}
		self.dataset[moduleName + 'Init'] = true;

		const popup = self;

		popup.addEventListener('click', (event) => {
			const element = event.target.closest('.close-popup');
			if (!element) {
				return;
			}

			event.preventDefault();
			event.stopPropagation();

			popupClose(popup);
		});
	});

	Array.from(document.querySelectorAll('.popup-link')).forEach((self) => {
		if (self.dataset[moduleName + 'Init']) {
			return;
		}
		self.dataset[moduleName + 'Init'] = true;

		self.addEventListener('click', (event) => {

			const popupName = self.getAttribute('href').replace('#', '');

			const currentPopup = document.querySelector('.popup_' + popupName);
			if (self.getAttribute('data-video')) {
				let container = currentPopup.querySelector('.video-container');
				container.innerHTML = `<iframe width="auto" height="315" src="${self.getAttribute(
					'data-video')}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;
			}

			popupOpen(currentPopup, false);

			event.preventDefault();
		});
	});
}

window.popupsInit();

function popupOpen(popup, noCloseActivePopup = true) {

	if (!(popup && unlock)) {
		return;
	}

	const popupActive = document.querySelector('.popup.open');
	if (!noCloseActivePopup && popupActive) {
		popupClose(popupActive, false);
	} else {
		bodyLock();
	}

    let ajax = popup.dataset['ajax'];
    let url = popup.dataset['url'];
    if (ajax && url) {
        BX.ajax.insertToNode(url, popup.id);
        setTimeout(function () {
            popup.classList.add('open');
            initForm();
            initAjaxForms(popup);
        }, timeToLoadNodes);
    } else {
        popup.classList.add('open');
    }
}

window.popupOpen = popupOpen;

function popupClose(popup, doUnlock = true) {

	if (!unlock) {
		return;
	}

	popup.classList.remove('open');

	let ajax = popup.dataset['ajax'];
	if (ajax) {
		setTimeout(function () {
			popup.innerHTML = '';
		}, timeout);
	}

	if (doUnlock) {
		bodyUnLock();
	}
}

window.popupClose = popupClose;

function initAjaxForms(popup) {
    initForm();
    let formSelector = `#${popup.id}`
    let alert = $('.error_request');
    $(formSelector).on('submit', 'form[data-form-ajax]', function (e) {
        e.preventDefault();
        if($('input').hasClass("_error")){
            return;
        }
        $(alert).slideUp("slow");
        let form = this;
        const data = new FormData(form);

        $.ajax({
            url: form.action,
            type: 'post',
            data: data,
            processData: false,
            contentType: false,
            dataType: "html",
        }).done(function (response) {
            let error = $(response).find('.errortext').html();
            if (response !== null && error) {
                statusFormError(alert, error);
            } else {
                statusFormSuccess(formSelector, response);
            }
        })
    });

    function statusFormError(alert,error) {
        $(alert).html(error);
        $(alert).slideDown("slow");
        setTimeout(function () {
            rerenderRecapcha();
        },timeout);
        return;
    }

    function statusFormSuccess(formSelector, response) {
        $(formSelector).html(response);
    }

    function rerenderRecapcha() {
        let c = $('.g-recaptcha').length;
        for (var i = 0; i <= c; i++)
            grecaptcha.reset(i);
    }

}

window.initAjaxForms = initAjaxForms;

function bodyLock() {
	const lockPaddingValue = window.innerWidth - document.querySelector('.wrapper').offsetWidth + 'px';

	if (lockPadding.length > 0) {
		for (let index = 0; index < lockPadding.length; index++) {
			const el = lockPadding[index];
			el.style.paddingRight = lockPaddingValue;
		}
	}
	body.style.paddingRight = lockPaddingValue;
	body.classList.add('lock-popup');

	unlock = false;
	setTimeout(function () {
		unlock = true;
	}, timeout);
}

function bodyUnLock() {
	setTimeout(function () {
		if (lockPadding.length > 0) {
			for (let index = 0; index < lockPadding.length; index++) {
				const el = lockPadding[index];
				el.style.paddingRight = '0px';
			}
		}
		body.style.paddingRight = '0px';
		body.classList.remove('lock-popup');
	}, timeout);

	unlock = false;
	setTimeout(function () {
		unlock = true;
	}, timeout);
}

document.addEventListener('keydown', function (e) {
	if (e.which === 27) {
		const popupActive = document.querySelector('.popup.open');
		popupClose(popupActive);
	}
});

(function () {
	// проверяем поддержку
	if (!Element.prototype.closest) {
		// реализуем
		Element.prototype.closest = function (css) {
			var node = this;
			while (node) {
				if (node.matches(css)) {
					return node;
				} else {
					node = node.parentElement;
				}
			}
			return null;
		};
	}
})();

function popupOpenUniversalForm(title = '', text = '', noCloseActivePopup = false, button ='') {
    let modal = document.getElementById('universal-popup');
    let btn = modal.querySelector('[data-btn]');
	if(!btn.classList.contains('universal-message__button')){
		btn.classList.toggle('universal-message__button').innerHTML = 'OK';
	}
    let titleModal = modal ? modal.querySelector('.universal-message__title') : null;
    let textModal = modal ? modal.querySelector('.universal-message__text') : null;
    let textButton = modal ? modal.querySelector('.universal-message__button') : null;

    titleModal.innerHTML = title;
    if (titleModal) {
        titleModal.innerHTML = title;
    }
    if (textModal) {
        textModal.innerHTML = text;
    }
    if (textButton) {
        textButton.innerHTML = button;
		textButton.removeAttribute("class");
    }

    if (modal) {
        popupOpen(modal, noCloseActivePopup);
    }
}

window.popupOpen = popupOpenUniversalForm;

function popupAuthOpen() {
	const auth = document.querySelector('.popup_authorization');

	if (auth) {
		popupOpen(auth);
	}
}

window.popupAuthOpen = popupAuthOpen;

function popupNotAvailable() {
	const notAvailable = document.querySelector('.popup_not-available');

	if (notAvailable) {
		popupOpen(notAvailable);
	}
}

window.popupForgotPass = popupForgotPass;

function popupForgotPass() {
	const forgotPass = document.querySelector('.popup_forgot-pass');

	if (forgotPass) {
		popupOpen(forgotPass);
	}
}

window.popupImport = popupImport;

function popupImport() {
	const importPopup = document.querySelector('.popup_import');

	if (importPopup) {
		popupOpen(importPopup);
	}
}

window.popupNotAvailable = popupNotAvailable;

function copyOrder(id) {
	window.location.href = '/orders/?COPY_ORDER=Y&ID=' + id;
}

function popupCleanCart() {
	const cleanCart = document.querySelector('.popup_clean-cart');
	const cleanCartBtns = document.querySelectorAll('.popup_clean-cart ._btn');

	const url_string = window.location.href;
	const url = new URL(url_string);
	const id = url.searchParams.get("id");

	if (cleanCart) {
		popupOpen(cleanCart);

		if (id) {
			cleanCartBtns.forEach((button) => {
				button.addEventListener('click', () => {
					copyOrder(id);
				});
			});
		}
	} else if (id) {
		copyOrder(id);
	}
}

window.popupCleanCart = popupCleanCart;

function popupRegOpen() {
	const reg = document.querySelector('.popup_registration');

	if (reg) {
		popupOpen(reg);
	}
}

window.popupRegOpen = popupRegOpen;

function popupAnalogGoods() {
	const analog = document.querySelector('.popup_analog-goods');

	if (analog) {
		popupOpen(analog);
	}
}

window.popupAnalogGoods = popupAnalogGoods;

function popupPassChange() {
	const passChange = document.querySelector('.popup_pass-change');

	if (passChange) {
		popupOpen(passChange);
	}
}

window.popupPassChange = popupPassChange;

function popupWarningMessage() {
	const warningMessage = document.querySelector('.popup_warning-message');

	if (warningMessage) {
		popupOpen(warningMessage);
	}
}

window.popupWarningMessage = popupWarningMessage;

(function () {
	// проверяем поддержку
	if (!Element.prototype.matches) {
		// определяем свойство
		Element.prototype.matches = Element.prototype.matchesSelector ||
			Element.prototype.webkitMatchesSelector ||
			Element.prototype.mozMatchesSelector ||
			Element.prototype.msMatchesSelector;
	}
})();
/* eslint-enable */

const ranges = document.querySelectorAll('.profile-balance-input-range');

function handleInputChange(e) {
  const target = e.target;
  const min = target.min;
  const max = target.max;
  const val = target.value;

  target.style.backgroundSize = `${((val - min) * 100) / (max - min)}% 100%`;
}

function initRange(rangeBlock) {
  const inputRange = rangeBlock.querySelector('.input-range');
  const rangeRub = rangeBlock.querySelector('.rubs');
  const rangeScore = rangeBlock.querySelector('.score');
  const coef = inputRange.getAttribute('data-coef');

  rangeRub.innerHTML = inputRange.value;
  rangeScore.innerHTML = inputRange.value * coef;

  inputRange.oninput = () => {
    rangeRub.innerHTML = inputRange.value;
    rangeScore.innerHTML = inputRange.value * coef;
  };
}

ranges.forEach((oninput) => {
  oninput.addEventListener('input', handleInputChange);
});

ranges.forEach((range) => {
  initRange(range);
});

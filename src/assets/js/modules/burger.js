/* eslint-disable camelcase */
let unlock = true;
// function menu_close() {
//   const iconMenu = document.querySelector('.icon-menu');
//   const menuBody = document.querySelector('.menu__body');
//   iconMenu.classList.remove('_active');
//   menuBody.classList.remove('_active');
// }
//= ================
// BodyLock
function body_lock_remove(delay) {
  const body = document.querySelector('body');
  if (unlock) {
    const lock_padding = document.querySelectorAll('._lp');
    setTimeout(() => {
      for (let index = 0; index < lock_padding.length; index++) {
        const el = lock_padding[index];
        el.style.paddingRight = '0px';
      }
      body.style.paddingRight = '0px';
      body.classList.remove('_lock');
    }, delay);

    unlock = false;
    setTimeout(() => {
      unlock = true;
    }, delay);
  }
}
function body_lock_add(delay) {
  const body = document.querySelector('body');
  if (unlock) {
    const lock_padding = document.querySelectorAll('._lp');
    for (let index = 0; index < lock_padding.length; index++) {
      const el = lock_padding[index];
      el.style.paddingRight = `${
        window.innerWidth - document.querySelector('.wrapper').offsetWidth
      }px`;
    }
    body.style.paddingRight = `${
      window.innerWidth - document.querySelector('.wrapper').offsetWidth
    }px`;
    body.classList.add('_lock');

    unlock = false;
    setTimeout(() => {
      unlock = true;
    }, delay);
  }
}
function body_lock(delay) {
  const body = document.querySelector('body');
  if (body.classList.contains('_lock')) {
    body_lock_remove(delay);
  } else {
    body_lock_add(delay);
  }
}

const iconMenu = document.querySelector('.icon-menu');
if (iconMenu != null) {
  const delay = 500;
  const menuBody = document.querySelector('.menu__body');
  iconMenu.addEventListener('click', () => {
    if (unlock) {
      body_lock(delay);
      iconMenu.classList.toggle('_active');
      menuBody.classList.toggle('_active');
    }
  });
}

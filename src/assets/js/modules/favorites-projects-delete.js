const deleteFavoritesProjectsItems = document.querySelectorAll(
  '.item-favorites-projects__action'
);

deleteFavoritesProjectsItems.forEach((deleteItem) => {
  deleteItem.addEventListener('click', () => {
    deleteItem.classList.toggle('_active');
  });
});

const deleteIconButtons = document.querySelectorAll('.delete-icon');

deleteIconButtons.forEach((deleteButton) => {
  deleteButton.addEventListener('click', () => {
    const deleteButtonParent = deleteButton.closest('.product-card ');
    deleteButtonParent.remove();
  });
});

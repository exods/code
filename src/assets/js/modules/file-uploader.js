class FilesUploader {
  constructor(filesContainer) {
    this.filesContainer = filesContainer;
    this.previews = filesContainer.querySelector('.files-uploader__previews');
    this.input = filesContainer.querySelector('input');
    this.files = [];

    this.init();
  }

  getFilelist() {
    const fileList = new ClipboardEvent('').clipboardData || new DataTransfer();

    this.files.forEach((file) => {
      fileList.items.add(file);
    });

    return fileList.files;
  }

  addEvents() {
    this.input.addEventListener('change', () => {
      this.files.push(...this.input.files);
      this.updatePreview();
      this.updateInputValue();
    });
  }

  updateInputValue() {
    this.input.value = '';
    this.input.files = this.getFilelist();
    this.input.customFiles = this.files;
  }

  updatePreview() {
    this.previews.innerHTML = '';

    this.files.forEach((file) => {
      const url = URL.createObjectURL(file);

      this.previews.insertAdjacentHTML(
        'beforeend',
        `
            <div class="files-uploader__preview">
                <div class="files-uploader__preview-close">
                  <svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M10.2734 9.12822L9.13371 10.268L5.13905 6.27468L1.14219 10.2719L0.00127276 9.13096L3.99793 5.13396L0.00157065 1.13899L1.1413 -0.000732372L5.1376 3.99418L9.13142 1.2038e-05L10.2723 1.14093L6.27872 5.1349L10.2734 9.12822Z" fill="white"/>
                  </svg>                
                </div>
                <img src="${url}" alt="">
            </div>
          `
      );
    });

    const previews = this.previews.querySelectorAll('.files-uploader__preview');
    previews.forEach((preview, index) => {
      this.addEventRemoveFile(preview, index);
    });
  }

  removeFile(index) {
    this.files.splice(index, 1);
    this.updateInputValue();
    this.updatePreview();
  }

  addEventRemoveFile(preview, index) {
    const closeButton = preview.querySelector('.files-uploader__preview-close');

    closeButton.addEventListener('click', () => {
      this.removeFile(index);
    });
  }

  init() {
    this.addEvents();
  }
}

function initfilesUploaders() {
  const filesUploaders = document.querySelectorAll('.files-uploader');

  filesUploaders.forEach((filesUploader) => {
    new FilesUploader(filesUploader);
  });
}

initfilesUploaders();

window.initfilesUploaders = initfilesUploaders;

/* eslint-disable */
let tabs = document.querySelectorAll("._tabs");
for (let index = 0; index < tabs.length; index++) {
  let tab = tabs[index];
  let tabs_items = tab.querySelectorAll('._tabs-item:not([data-prevent-animation="y"])');
  let tabs_blocks = tab.querySelectorAll('._tabs-block');
  for (let index = 0; index < tabs_items.length; index++) {
    let tabs_item = tabs_items[index];
    tabs_item.addEventListener("click", function (e) {
      for (let index = 0; index < tabs_items.length; index++) {
        let tabs_item = tabs_items[index];
        tabs_item.classList.remove('_open');
        tabs_blocks[index].classList.remove('_open');
      }
      tabs_item.classList.add('_open');
      tabs_blocks[index].classList.add('_open');
      e.preventDefault();
    });
  }
}

let tabsSubmenu = document.querySelectorAll("._tabs-submenu");
for (let index = 0; index < tabsSubmenu.length; index++) {
  let tabSubmenu = tabsSubmenu[index];
  let tabsSubmenu_items = tabSubmenu.querySelectorAll("._tabs-item");
  let tabsSubmenu_blocks = tabSubmenu.querySelectorAll("._tabs-block");
  for (let index = 0; index < tabsSubmenu_items.length; index++) {
    let tabsSubmenu_item = tabsSubmenu_items[index];
    tabsSubmenu_item.addEventListener("mouseenter", function (e) {
      for (let index = 0; index < tabsSubmenu_items.length; index++) {
        let tabsSubmenu_item = tabsSubmenu_items[index];
        tabsSubmenu_item.classList.remove('_active');
        tabsSubmenu_blocks[index].classList.remove('_active');
      }
      tabsSubmenu_item.classList.add('_active');
      tabsSubmenu_blocks[index].classList.add('_active');
      e.preventDefault();
    });
  }
}
function initTabCharacteristics() {
  let tabsCharacteristics = document.querySelectorAll(".tabs-characteristics");
  for (let index = 0; index < tabsCharacteristics.length; index++) {
    let tab = tabsCharacteristics[index];
    let tabsCharacteristics_items = tab.querySelectorAll(".tabs-characteristics__item");
    let tabsCharacteristics_blocks = tab.querySelectorAll(".tabs-characteristics__block");
    for (let index = 0; index < tabsCharacteristics_items.length; index++) {
      let tabsCharacteristics_item = tabsCharacteristics_items[index];
      tabsCharacteristics_item.addEventListener("click", function (e) {
        for (let index = 0; index < tabsCharacteristics_items.length; index++) {
          let tabsCharacteristics_item = tabsCharacteristics_items[index];
          tabsCharacteristics_item.classList.remove('_open');
          tabsCharacteristics_blocks[index].classList.remove('_open');
        }
        tabsCharacteristics_item.classList.add('_open');
        tabsCharacteristics_blocks[index].classList.add('_open');
        e.preventDefault();
      });
    }
  }
}

initTabCharacteristics();
window.initTabCharacteristics = initTabCharacteristics;
/* eslint-enable */

// function openTab(id) {

// }
// window.openTab = openTab;

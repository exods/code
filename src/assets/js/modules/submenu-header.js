// const subHeadersMenu = document.querySelectorAll('.submenu-body');

// function openSubMenu() {
//     el.classList.add('submenu-body--open');
// }

// function closeSubMenu() {
//     el.classList.remove('submenu-body--open');
// }

// subHeadersMenu.forEach((el) =>
//   el.addEventListener('mouseover', () => {
//     openSubMenu()
//   });
//   el.addEventListener('mouseout', () => {
//     closeSubMenu()
//   });
// );
const menuParents = document.querySelectorAll('.submenu');
const vw = Math.max(
  document.documentElement.clientWidth || 0,
  window.innerWidth || 0
);

menuParents.forEach((menuParent) => {
  const pageForMenu = document.querySelector('.page');
  menuParent.addEventListener('mouseenter', () => {
    menuParent.classList.add('_submenu--open');
    pageForMenu.classList.add('_show-orevlay');
  });
  menuParent.addEventListener('mouseleave', () => {
    menuParent.classList.remove('_submenu--open');
    pageForMenu.classList.remove('_show-orevlay');
  });
});

[...document.querySelectorAll('.js-submenu-tab')].forEach((tab) => {
  tab.addEventListener('click', function () {
    const id = this.dataset.tabSubMenu;
    this.parentNode
      .querySelectorAll('.js-submenu-tab')
      .forEach((item) => item.classList.remove('active'));
    this.classList.add('active');

    const submenuContent = this.closest('.js-submenu-content');
    submenuContent
      .querySelectorAll('.js-submenu-list')
      .forEach((item) => item.classList.remove('active'));
    submenuContent
      .querySelector(`.js-submenu-list[data-tab-sub-menu="${id}"]`)
      .classList.add('active');
  });
});

if (vw <= 1150) {
  const menuParentsLinks = document.querySelectorAll(
    '.submenu .menu-bottom-header__link'
  );
  menuParentsLinks.forEach((menuParentsLink) => {
    menuParentsLink.addEventListener('click', function _tmp(e) {
      e.preventDefault();
      this.removeEventListener('click', _tmp);
    });
  });
}

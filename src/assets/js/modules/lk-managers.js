import queryString from 'query-string';

const scrollToElement = require('scroll-to-element');

function openTabProduct(name) {
  const managerTab = document.querySelector('._tabs-managers');
  const tabsTriggers = Array.from(managerTab.querySelectorAll('._tabs-item'));
  const tabsContents = managerTab.querySelectorAll('._tabs-block');
  tabsTriggers.forEach((trigger) => {
    trigger.classList.remove('_open');
  });
  tabsContents.forEach((content) => {
    content.classList.remove('_open');
  });
  const tabIndex = tabsTriggers.findIndex((item) => item.dataset.name === name);
  if (tabIndex !== -1) {
    tabsTriggers[tabIndex].classList.add('_open');
    tabsContents[tabIndex].classList.add('_open');
  }
}

function scrollTo(selector, id) {
  const reviewsItems = Array.from(document.querySelectorAll(selector));
  const reviewNumber = reviewsItems.find(
    (item) => parseInt(item.getAttribute('data-id'), 10) === id
  );
  if (reviewNumber) {
    reviewNumber.classList.add('_animate');
    scrollToElement(reviewNumber, {
      offset: -152,
      align: 'top',
      duration: 1000,
    });
  }
}

function parseAddress() {
  return queryString.parse(window.location.search);
}

function scrollToComment() {
  const parsed = parseAddress();

  if (parsed['data-id']) {
    switch (parsed.tab) {
      case 'unpublished_review':
        setTimeout(
          () => scrollTo('.scroll-review', parseInt(parsed['data-id'], 10)),
          700
        );
        console.log(222);

        break;

      case 'unpublished_mounting':
        setTimeout(
          () => scrollTo('.scroll-mounting', parseInt(parsed['data-id'], 10)),
          700
        );
        break;

      default:
        setTimeout(
          () => scrollTo('.scroll-review', parseInt(parsed['data-id'], 10)),
          1000
        );
    }
  }
}

const isProfileComments = document.querySelector(
  '.profile-wrapper .profile-addition'
);
if (isProfileComments) {
  scrollToComment();
}

window.parseAddress = parseAddress;
window.scrollToComment = scrollToComment;
window.openTabProduct = openTabProduct;

const names = document.querySelectorAll('.comment-top--name');

function addInitial(item) {
  item.previousSibling.previousSibling.innerHTML = item.textContent.charAt(0);
}

names.forEach(addInitial);

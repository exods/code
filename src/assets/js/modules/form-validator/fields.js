import validator from 'validator';

/**
 * Создает экземпляр Field.
 *
 * @constructor
 * @this  {Field}
 * @param {field} r - HTML элемент input.
 */
class Field {
  constructor(field) {
    this.field = field;
    this.errorClass = '_error';

    this.init();
  }

  /** @private */
  init() {
    this.addEventListeners();
  }

  /** @private */
  addEventListeners() {
    this.field.addEventListener('focus', () => {
      this.removeError();
    });
    this.field.addEventListener('change', () => {
      this.removeError();
    });
  }

  addError() {
    this.field.classList.add(this.errorClass);
  }

  removeError() {
    this.field.classList.remove(this.errorClass);
  }

  validation() {
    switch (this.field.getAttribute('data-required')) {
      case 'text':
        if (this.field.value) {
          this.removeError();
          return true;
        }
        break;
      case 'checkbox':
        if (this.field.checked) {
          this.removeError();
          return true;
        }
        break;
      case 'tel':
        if (this.field.value !== '' && this.field.value.indexOf('_') === -1) {
          this.removeError();
          return true;
        }
        break;
      case 'email':
        if (validator.isEmail(this.field.value)) {
          this.removeError();
          return true;
        }
        break;
      case 'file':
        if (this.field.value) {
          this.removeError();
          return true;
        }
        break;
      default:
        return true;
    }
    this.addError();
    return false;
  }
}

export default Field;

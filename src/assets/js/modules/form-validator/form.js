import Field from './fields';

/**
 * Создает экземпляр Form.
 *
 * @constructor
 * @this  {Form}
 * @param {form} r - HTML элемент формы.
 */
class Form {
  constructor(form) {
    this.form = form;
    this.fields = [];

    this.init();
  }

  /** @private */
  init() {
    this.initFields();
    this.addEventListeners();
    this.setGlobalMethodsForm();
  }

  /** @private */
  initFields() {
    const inputs = this.form.querySelectorAll('input[data-required]');

    inputs.forEach((item) => {
      this.fields.push(new Field(item));
    });
  }

  /** @private */
  addEventListeners() {
    this.form.addEventListener('submit', (e) => {
      if (!this.validationForm()) {
        e.preventDefault();
        this.scrollToTop();
      }
    });
  }

  /** @private */
  setGlobalMethodsForm() {
    const self = this;
    this.form.validationForm = () => self.validationForm();
    this.form.resetValidationForm = () => self.resetValidationForm();
  }

  validationForm() {
    return this.fields.reduce((accumulator, field) => {
      const resultValiudation = field.validation();
      return accumulator ? resultValiudation : false;
    }, true);
  }

  resetValidationForm() {
    this.fields.forEach((field) => {
      field.removeError();
    });
  }

  scrollToTop() {
    this.form.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
    });
  }
}

export default Form;

const searchHeader = document.querySelector('.search-header__item');
const searchHeaderClose = document.querySelector('.drop-search-header__close');
const searchHeaderInput = document.querySelector('#title-search-input');
if (searchHeader && searchHeaderClose && searchHeaderInput) {
  searchHeader.addEventListener('click', () => {
    searchHeader.parentNode.classList.add('_open');
    setTimeout(() => {
      searchHeaderInput.focus();
    }, 300);
  });
  searchHeaderClose.addEventListener('click', () => {
    if (searchHeader.parentNode.classList.contains('_open')) {
      searchHeader.parentNode.classList.remove('_open');
    }
  });
}

[...document.querySelectorAll('.drop-search-header')].forEach((item) =>
  item.addEventListener('click', (e) => {
    if (!e.target.closest('.drop-search-header__content')) {
      searchHeader.parentNode.classList.remove('_open');
    }
  })
);

const searchMainInput = document.querySelector('._faq__search--input');
const searchMainParent = document.querySelector('._faq__search');
const searchMainClear = document.querySelector('._faq__search--clear');
if (searchMainInput && searchMainParent && searchMainClear) {
  searchMainInput.addEventListener('keyup', (e) => {
    if (e.target.value !== '') {
      searchMainParent.classList.add('_is-clear');
    } else {
      searchMainParent.classList.remove('_is-clear');
    }
  });
  searchMainClear.addEventListener('click', () => {
    searchMainInput.value = '';
    searchMainParent.classList.remove('_is-clear');
  });
}

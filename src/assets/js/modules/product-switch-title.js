const headProduct = document.querySelector('.head-product');
const productDescription = document.querySelector('.nav-product--description');
const productLinks = document.querySelectorAll('.nav-product li');
function switchTitle() {
  if (productDescription.classList.contains('_open')) {
    headProduct.classList.add('_switch');
  } else {
    headProduct.classList.remove('_switch');
  }
}

productLinks.forEach((link) => {
  link.addEventListener('click', switchTitle);
});

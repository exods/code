const videos = document.querySelectorAll('.js-video-init');

videos.forEach((video) => {
  const url = video.getAttribute('data-video-url');
  const elementsInit = video.querySelectorAll('.launch-video');
  // const selectorInit = video.getAttribute('data-init-selector');
  // const elementsInit = document.querySelectorAll(selectorInit);
  let videoIsInit = false;

  elementsInit.forEach((el) => {
    el.addEventListener('click', () => {
      if (!videoIsInit) {
        setTimeout(() => {}, 500);
        video.parentNode.insertAdjacentHTML(
          'afterBegin',
          `<iframe src="${url}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
        );
        videoIsInit = true;
        setTimeout(() => {
          video.remove();
        }, 2000);
      }
    });
  });
});

const deletesFavoriteSpecifications = document.querySelectorAll(
  '.favorites-specifications__delete'
);

deletesFavoriteSpecifications.forEach((deleteItemSpecifications) => {
  deleteItemSpecifications.addEventListener('click', () => {
    const deleteItemSpecificationsParent = deleteItemSpecifications.closest(
      '.favorites-specifications__item'
    );
    deleteItemSpecificationsParent.remove();
  });
});

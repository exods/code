const dropMenuButtonCatalog = document.querySelectorAll(
  '.side-lvl-two__toggle'
);

dropMenuButtonCatalog.forEach((el) => {
  el.classList.toggle('render');
  el.addEventListener('click', () => {
    el.parentNode.classList.toggle('_active');
  });
});

function initEffectButton(button, evts = ['mouseenter', 'mouseout']) {
  evts.forEach((evt) => {
    button.addEventListener(evt, (e) => {
      const parentOffset = button.getBoundingClientRect();
      const relX = e.pageX - parentOffset.left;
      const relY = e.clientY - parentOffset.top;

      const span = button.getElementsByTagName('span');
      if (span) {
        span[0].style.top = `${relY}px`;
        span[0].style.left = `${relX}px`;
      }
    });
  });
}

window.initEffectButton = initEffectButton;

export default initEffectButton;

const fadeOut = function (el) {
  let opacity = 1;

  const timer = setInterval(() => {
    if (opacity <= 0.1) {
      clearInterval(timer);
      el.style.display = 'none';
    }

    el.style.opacity = opacity;

    opacity -= opacity * 0.1;
  }, 10);
};

const fadeIn = function (el) {
  let opacity = 0.01;

  el.style.display = 'block';

  const timer = setInterval(() => {
    if (opacity >= 1) {
      clearInterval(timer);
    }

    el.style.opacity = opacity;

    opacity += opacity * 0.1;
  }, 10);
};

export { fadeOut, fadeIn };

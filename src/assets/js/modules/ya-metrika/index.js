const code = 86243033;

export default function yaMetrika(typeEvent) {
    if (window.ym) {
        window.ym(code, 'reachGoal', typeEvent);
    } else {
        setTimeout(() => {
            yaMetrika(typeEvent)
        }, 1000);
    }
}

export const ECOMMERCE = {
    purchase(products) {
        /*
        [
            {
                "url": "/catalog/elektroustanovochnye-izdeliya/universalnaya-seriya-eui-avanti/izdeliya-skrytogo-montazha/vanilnaya-dymka/vyklyuchateli-pereklyuchateli-invertory/vyklyuchatel-odnoklavishnyy-v-stenu-avanti-vanilnaya-dymka/",
                "img": "/upload/resize_cache/iblock/cd1/160_160_1/cd1154aa2fdc3f74153aa65498a760a4.JPG",
                "id": "424189",
                "name": "Выключатель одноклавишный в стену, \"Avanti\", \"Ванильная дымка\"",
                "code": "4405103",
                "count": 1,
                "price": 466,
                "onOrder": 0
            }
        ]
         */

        dataLayer.push({
            "ecommerce": {
                "currencyCode": "RUB",
                "purchase": {
                    "products": products.map((product) => ({
                        "id": product.code,
                        "name": product.name,
                        "price": product.price,
                        // "brand": "Название бренда",
                        // "category": "Одежда/Мужская одежда/Толстовки и свитшоты",
                        // "variant": "Оранжевый цвет",
                        "quantity": product.count
                    }))
                }
            }
        })
    }
}

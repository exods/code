/* eslint-disable */
const inputNumbers = document.querySelectorAll('.calc-product');

class InputNumber {
  constructor(inputContainer) {
    this._inputContainer = inputContainer;
    this._input = this._inputContainer
      ? this._inputContainer.querySelector('input')
      : null;
    this._maxValue = this._input ? this._input.getAttribute('data-max') : 100;
    this._minValue = this._input ? this._input.getAttribute('data-min') : 1;
    this._buttonTop = this._inputContainer
      ? this._inputContainer.querySelector('.calc-product__more')
      : null;
    this._buttonBottom = this._inputContainer
      ? this._inputContainer.querySelector('.calc-product__less')
      : null;

    this.init();
  }

  _validValue(value) {
    const intValue = parseInt(value);

    if (!isNaN(intValue)) {
      if (intValue < this._minValue) {
        return this._minValue;
      }

      if (intValue > this._maxValue) {
        return this._maxValue;
      }

      return parseInt(value);
    }

    return 1;
  }

  _getValue() {
    return this._input.value;
  }

  _setValue(value) {
    this._input.value = value;
  }

  _increment() {
    const value = this._validValue(parseInt(this._getValue()) + 1);

    this._setValue(value);
  }

  _decrement() {
    const value = this._validValue(parseInt(this._getValue()) - 1);

    this._setValue(value);
  }

  _addEventListenersButtons() {
    this._buttonTop.addEventListener('click', () => {
      this._increment();
    });
    this._buttonBottom.addEventListener('click', () => {
      this._decrement();
    });

    const self = this;
    let timeoutButtonTop = null;
    let timeoutButtonBottom = null;

    this._buttonTop.addEventListener('mousedown', () => {
      timeoutButtonTop = setInterval(() => this._increment(), 200);
    });
    document.addEventListener('mouseup', () => {
      clearInterval(timeoutButtonTop);
    });
    this._buttonBottom.addEventListener('mousedown', () => {
      timeoutButtonBottom = setInterval(() => this._decrement(), 200);
    });
    document.addEventListener('mouseup', () => {
      clearInterval(timeoutButtonBottom);
    });
  }

  _addEventListenerInput() {
    this._input.addEventListener('input', () => {
      this._input.value = this._validValue(this._input.value);
    });
  }

  init() {
    this._setValue(this._validValue(this._input.value));
    this._addEventListenersButtons();
    this._addEventListenerInput();
  }
}

inputNumbers.forEach((inputNumber) => {
  new InputNumber(inputNumber);
});
/* eslint-enable */

import * as textMaskCore from './textMaskCoreModul/textMaskCoreModul';

const phoneMask = document.querySelectorAll('.phone-mask');

phoneMask.forEach((item) => {
  const mask = textMaskCore.createTextMaskInputElement({
    inputElement: item,
    mask: [
      '+',
      '7',
      ' ',
      '(',
      /[1-9]/,
      /\d/,
      /\d/,
      ')',
      ' ',
      /\d/,
      /\d/,
      /\d/,
      '-',
      /\d/,
      /\d/,
      '-',
      /\d/,
      /\d/,
    ],
  });

  item.addEventListener('input', (e) => {
    // если маска еще используется
    if (!/\+7/.test(e.target.value)) {
      // если маска еще не используется и пытаются написать 7 или 8
      if (/7|8/.test(e.target.value)) {
        e.target.value = e.target.value.replace(/7|8/, '+');
      }
    }

    mask.update();
  });
});

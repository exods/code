import queryString from 'query-string';

const userLocation = queryString.parse(window.location.hash);

if (userLocation.modal === 'auth') {
  window.popupAuthOpen();
}

if (userLocation.modal === 'register') {
  window.popupRegOpen();
}

if (userLocation.modal === 'cleanCart') {
  window.popupCleanCart();
}

if (userLocation.modal === 'notAvailable') {
  window.popupNotAvailable();
}

if (userLocation.modal === 'importPopup') {
  window.popupImport();
}

if (userLocation.modal === 'forgotPass') {
  window.popupForgotPass();
}

if (userLocation.modal === 'passChange') {
  window.popupPassChange();
}

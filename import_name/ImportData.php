<?


class ImportData
{
    private $filePath = '';
    private $IBLOCK = '';
    private $data = array();
    private $FILE_BACKUP = 'BACKUP';
    private $FILE_LOGGER_NAME = '';


    /**
     * @param string $filePath
     * @param integer $IBLOCK
     */
    public function __construct(string $filePath, int $IBLOCK, string $FILE_LOGGER_NAME)
    {
        $this->filePath = $filePath;
        $this->IBLOCK = $IBLOCK;
        $this->FILE_LOGGER_NAME = $FILE_LOGGER_NAME;
    }

    public function writeIblock()
    {
        $this->setLogs('Начало импорта');
        mkdir('./error/');
        $fp = fopen('error/IBLOCK-' . $this->IBLOCK . '_' . date("d.m.Y") . '.csv', 'w');
        $IblockArr = $this->readIblock();
        $arCSV = $this->readFile();
        $element = new CIBlockElement;
        foreach ($IblockArr as $key => $value) {
            if (array_key_exists($key, $arCSV)) {
                $result = $element->Update($IblockArr[$key]['ID'], [
                    "NAME" => $arCSV[$key]
                ]);
                if ($result) {
                    echo 'Было - ' . $IblockArr[$key]['NAME'] . '   | Поменяли на - ' . $arCSV[$key] . '<br>';
                } else {
                    $this->setLogs($element->LAST_ERROR);
                }
            } else {

                $this->setLogs('Элемент ' . $IblockArr[$key]['NAME'] . ' не найден');
                file_put_contents('error/IBLOCK-' . $this->IBLOCK . '_' . date("d.m.Y") . '.csv',   $IblockArr[$key]['NAME'] .';'. $key . ';' .  $IblockArr[$key]['ID']  . PHP_EOL, FILE_APPEND);
            }
        }
        fclose($fp);
        $this->setLogs('Конец импорта');
    }

    public function setLogs($PUT_STRING)
    {
        file_put_contents(__DIR__ . "/" . $this->FILE_LOGGER_NAME . '_' . date("Y-m-d") . '.txt', $PUT_STRING . '  ' . date("H:i:s") . PHP_EOL, FILE_APPEND);
        file_put_contents(__DIR__ . "/" . $this->FILE_LOGGER_NAME . '_' . date("Y-m-d") . '.txt', '======================================================================' . PHP_EOL, FILE_APPEND);
    }

    private function readIblock(): array
    {
        $this->setLogs('Начало считывания с ИБ' . '-' . $this->IBLOCK);

        $arElements = [];
        $arFilter = array("IBLOCK_ID" => $this->IBLOCK, "ACTIVE" => "Y");
        $res = CIBlockElement::GetList(
            [],
            $arFilter,
            false,
            [],
            ["ID", "IBLOCK_ID", "NAME", 'CODE']
        );
        while ($result = $res->Fetch()) {

            $arElements[$result["CODE"]]["NAME"] = $result["NAME"];
            $arElements[$result["CODE"]]["CODE"] = $result["CODE"];
            $arElements[$result["CODE"]]["ID"] = $result["ID"];

        }
        $this->setLogs('Конец считывания с ИБ' . '-' . $this->IBLOCK);

        return $arElements;

    }

    private function readFile(): array
    {
        $this->data = $this->getDataCSV();

        return $this->data;
    }

    private function getDataCSV(): array
    {
        $result = [];
        $num = '';
        if (($handle = fopen($this->getFilePath(), "r")) !== FALSE) {
            while (($result = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $num = count($result);
                for ($i = 0; $i < $num; $i++) {
                    $value[] = $result[$i];
                }
            }
            fclose($handle);
            $this->data = array_chunk($value, $num);
        }
        foreach ($this->data as $item) {

            $result[$this->getCode($item[0])] = $item[2];
        }
        return $result;
    }

    private function getFilePath(): string
    {
        return $this->filePath;
    }

    private function getCode($URL): string
    {

        $arURL = end(explode('/', parse_url(mb_substr($URL, 0, -1))['path']));
        return $arURL;
    }

    public function getIBLOCK(): int
    {
        return $this->IBLOCK;
    }

    public function debug($ar)
    {
        echo '<pre>';
        print_r($ar);
        echo '</pre>';
    }

    public function createBackup()
    {
        $this->setLogs('Создание резервной копии');

        mkdir('./backup/');
        $fp = fopen('backup/' . $this->FILE_BACKUP . '_IBLOCK-' . $this->IBLOCK . '_' . date("d.m.Y") . '.csv', 'w');
        $this->setLogs('Начало наполнения резервной копии');
        foreach ($this->readIblock() as $key => $fields) {
            fputcsv($fp, $fields, ';');

        }
        $this->setLogs('Конец наполнения резервной копии');

        fclose($fp);
    }

}
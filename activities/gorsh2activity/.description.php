<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arActivityDescription = array(
    'NAME' => GetMessage('TEST_DESCR_NAME'),
    'DESCRIPTION' => GetMessage('TEST_DESCR_DESCR'),
    'TYPE' => array('activity'),
    'CLASS' => 'gorsh2activity',
    'JSCLASS' => 'BizProcActivity',
    'CATEGORY' => array(
        'ID' => 'other',
    ),
    'RETURN' => array(
        'userGroupCode' => array(
            'NAME' => GetMessage('TEST_DESCR_DESCR'),
            'TYPE' => 'user',
            'DESCRIPTION' => GetMessage('TEST_DESCR_DESCR')
        ),
        'UserId' => array(
            'NAME' => GetMessage('TEST_USER_NAME'),
            'TYPE' => 'user',
            'DESCRIPTION' => GetMessage('TEST_DESCR_DESCR')
        )
    )
);

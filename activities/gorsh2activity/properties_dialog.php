<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\Localization\Loc;

?>


<tr>
    <?dump($arCurrentValues)?>
    <td align="right" width="40%">
        <?= Loc::getMessage('USER_GROUP_TITLE'); ?>
        <span style="color:#FF0000;">*</span> :
    </td>
    <td width="60%">
        <?= CBPDocument::ShowParameterField('string', 'userGroupCode', $arCurrentValues['userGroupCode'], array('size' => 50)) ?>
    </td>
</tr>
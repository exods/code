<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\UserGroupTable;
use Bitrix\Main\Localization\Loc;

class CBPgorsh2activity extends CBPActivity
{
    public function __construct($name)
    {
        parent::__construct($name);
        $this->arProperties = [
            'Title' => '',
            'userGroupCode' => '',
            'UserId' => ''
        ];
    }

    public function Execute()
    {
        return CBPActivityExecutionStatus::Closed;
    }

    public static function GetPropertiesDialog($documentType, $activityName,
                                               $arWorkflowTemplate,$arWorkflowParameters, $arWorkflowVariables,
                                               $arCurrentValues = null, $formName = '')
    {
        $runtime = CBPRuntime::GetRuntime();

        if (!is_array($arWorkflowParameters))
            $arWorkflowParameters = array();
        if (!is_array($arWorkflowVariables))
            $arWorkflowVariables = array();


        if (!is_array($arCurrentValues))
        {
            $arCurrentValues = array('userGroupCode' => '');

            $arCurrentActivity= &CBPWorkflowTemplateLoader::FindActivityByName(
                $arWorkflowTemplate,
                $activityName
            );
            if (is_array($arCurrentActivity['Properties']))
                $arCurrentValues['userGroupCode'] =
                    $arCurrentActivity['Properties']['userGroupCode'];
        }

        // properties_dialog.php в папке действия.
        return $runtime->ExecuteResourceFile(
            __FILE__,
            'properties_dialog.php',
            array(
                'arCurrentValues' => $arCurrentValues,
                'formName' => $formName,
                'arWorkflowTemplate' => $arWorkflowTemplate,
            )
        );
    }


    public static function GetPropertiesDialogValues($documentType, $activityName,
                                                     &$arWorkflowTemplate, &$arWorkflowParameters, &$arWorkflowVariables,
                                                     $arCurrentValues, &$arErrors)
    {

        $arErrors = array();

        if (strlen($arCurrentValues['userGroupCode']) <= 0)
        {

            $arErrors[] = [
                'code' => 'emptyCode',
                'message' => Loc::getMessage('ACTIVITY_EMPTY_TEXT'),
            ];
            return false;
        }
        $arProperties = [
            'userGroupCode' => $arCurrentValues['userGroupCode'],
            'UserId' => '',
            "group" => []
        ];

        $filter = ['=STRING_ID' => $arCurrentValues["userGroupCode"]];
        $groups = self::getUsers(['*'], $filter);
        if (!$group = $groups->Fetch()) {
            $filter = ["=ID" => 1];
            $groups = self::getUsers(['*'], $filter);
            $group = $groups->Fetch();
        }
        if ($group['ID']) {
            $arProperties['group'] = $group;
            $users = CUser::GetList('ID','ASC',['GROUPS_ID' => [$group['ID']]]);
            if ($user = $users->Fetch()) {
                $arProperties['UserId'] = $user['ID'];
            }
        }
        $arCurrentActivity = &CBPWorkflowTemplateLoader::FindActivityByName(
            $arWorkflowTemplate,
            $activityName
        );
        $arCurrentActivity['Properties'] = $arProperties;

        return true;
    }

    private function getUsers($select = ['*'], $filter = []) {
        return \Bitrix\Main\GroupTable::getList(['select' => $select,'filter' => $filter]);
    }
}
?>
<?php
include($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use App\Utils\ExportUsers;

/**
 * Служебная выгрузка сотрудников без фото на портале
 * TASK - 55918
 */
global $USER;
global $APPLICATION;
if (!$USER->IsAdmin()) {
    exit('У вас нет прав!');
}
$export = new ExportUsers();
$users = $export->init();
$APPLICATION->RestartBuffer();
Header('Content-Type: application/force-download');
Header('Content-Type: application/octet-stream');
Header('Content-Type: application/download');
Header('Content-Disposition: attachment;filename=users_all.xls');
Header('Content-Transfer-Encoding: binary');
if (count($users) > 0): ?>
<meta http-equiv="Content-type" content="text/html;charset=<?= LANG_CHARSET ?>"/>
<table border="1">
    <thead>
    <tr>
        <? foreach ($export->getSelectFields() as $field): ?>
            <td><?= $field ?></td>
        <? endforeach; ?>
    </tr>
    </thead>
    <tbody>
    <? foreach ($users as $user): ?>
        <tr>
            <? foreach ($user as $field => $value): ?>
                <td><?= ($value) ?: '-' ?></td>
            <? endforeach; ?>
        </tr>
    <?endforeach;
    endif ?>
    </tbody>
</table>
<? exit; ?>

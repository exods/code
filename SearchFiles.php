<?php

class SearchFiles
{
    private $ATR;
    private $DIR;
    private $LIMIT;
    private $FILE_PATH;
    private $FILE_ATTR;
    private $FILE_SIZE;
    private $WRITE_FILE;
    private $DAYS_LAST;


    private function getDaysLast($file)
    {
        $NOW = date_create_from_format('Y-m-d', date('Y-m-d'));
        $LAST = date_create_from_format('Y-m-d', date('Y-m-d', fileatime($file)));
        $DAYS = (array)date_diff($LAST, $NOW);
        return $DAYS['days'];
    }

    /**
     * @param int|mixed $DAYS_LAST
     */
    private function setDaysLast($DAYS_LAST): void
    {
        $this->DAYS_LAST = $DAYS_LAST;
    }


    /**
     * @return mixed
     */
    private function getFileSize($file)
    {

        if (!file_exists($file)) return "Файл  не найден";

        $filesize = filesize($file);

        if ($filesize > 1024) {
            $filesize = ($filesize / 1024);
            if ($filesize > 1024) {
                $filesize = ($filesize / 1024);
                if ($filesize > 1024) {
                    $filesize = ($filesize / 1024);
                    $filesize = round($filesize, 1);
                    $this->FILE_SIZE = $filesize . " ГБ";
                } else {
                    $filesize = round($filesize, 1);
                    $this->FILE_SIZE = $filesize . " MБ";
                }
            } else {
                $filesize = round($filesize, 1);
                $this->FILE_SIZE = $filesize . " Кб";
            }
        } else {
            $filesize = round($filesize, 1);
            $this->FILE_SIZE = $filesize . " байт";
        }


        return $this->FILE_SIZE;
    }

    private function setListPath($data)
    {
         //file_put_contents(__DIR__ . '/list-' . date("m-Y", fileatime($data)) . '.txt', $data . '(' . $this->getFileSize($data) . ')(' . $this->getDaysLast($data) . ' days)' . PHP_EOL, FILE_APPEND);
         file_put_contents(__DIR__ . '/list-' . date("m-Y", fileatime($data)).'(' . $this->getDaysLast($data) . ' days)' . '.txt', $data  . PHP_EOL, FILE_APPEND);
         file_put_contents(__DIR__ . '/list-old' . '.txt', $data  . PHP_EOL, FILE_APPEND);
    }

    /**
     * @param $ATR
     * @param $DIR
     * @param $LIMIT
     */
    public function __construct(array $ATR = ['png', 'jpg'], string $DIR = './', int $LIMIT = 100, bool $WRITE_FILE = false, $DAYS_LAST = 300)
    {
        $this->ATR = $ATR;
        $this->DIR = $DIR;
        $this->LIMIT = $LIMIT;
        $this->WRITE_FILE = $WRITE_FILE;
        $this->DAYS_LAST = $DAYS_LAST;
    }

    /**
     * @return mixed
     */
    public function buildSearch()
    {
        $full_size = 0;
        $it = new RecursiveDirectoryIterator($this->DIR);
        foreach (new RecursiveIteratorIterator($it) as $file) {
            clearstatcache();
            $info = new SplFileInfo(basename($file));
            $FILE = $info->getExtension();
            if (in_array($FILE, $this->ATR) && ($this->getFileSize($file) > $this->LIMIT) && ($this->DAYS_LAST < $this->getDaysLast($file))) {
                echo $file . '(' . $this->getFileSize($file) . ')' . PHP_EOL;
                if ($this->WRITE_FILE) {
                    $this->setListPath($file);
                    $full_size = $full_size + $this->getFileSize($file);
                    file_put_contents(__DIR__ . '/list-full.txt', round(($full_size / 1024) / 1024, 2) . ' ГБ');
                }
            }
        }
        file_put_contents(__DIR__ . '/logs/list-status.txt', 'finish');

    }

    /**
     * @param mixed $FILE_PATH
     */
    private function setFilePath($FILE_PATH): void
    {
        $this->FILE_PATH = $FILE_PATH;
    }

    /**
     * @return mixed
     */
    private function getFileAttr()
    {
        return $this->FILE_ATTR;
    }

    /**
     * @param mixed $FILE_ATTR
     */
    private function setFileAttr($FILE_ATTR): void
    {
        $this->FILE_ATTR = $FILE_ATTR;
    }


    /**
     * @return array
     */
    private function getAtr(): array
    {
        return $this->ATR;
    }


    /**
     * @param array $ATR
     */
    private function setAtr(array $ATR): void
    {
        $this->ATR = $ATR;
    }

    /**
     * @return string
     */
    private function getDir(): string
    {
        return $this->DIR;
    }

    /**
     * @param string $DIR
     */
    private function setDir(string $DIR): void
    {
        $this->DIR = $DIR;
    }

    /**
     * @return int
     */
    private function getLimit(): int
    {
        return $this->LIMIT;
    }

    /**
     * @param int $LIMIT
     */
    private function setLimit(int $LIMIT): void
    {
        $this->LIMIT = $LIMIT;
    }

    public function delFile(string $fullPath, bool $showProgress = false, bool $html = false)
    {
        $lineBreak = [
            'true' => '<br/>',
            'false' => PHP_EOL
        ];

        $dirsFiles = explode("\n", file_get_contents($fullPath));
        print_r($dirsFiles);
        foreach ($dirsFiles as $dirFile) {
            if ($showProgress) {
                echo trim($dirFile) . $lineBreak[$html];

            }
            unlink($dirFile);

        }
    }


}

$search = new SearchFiles(['jpg','png','gif'], $_SERVER['DOCUMENT_ROOT'] . "/upload/iblock", 10, true, 255);
//$search->buildSearch();
//$search->delFile('list-old.txt', true, true);

<?php
class Apipbd {
    
    private $login       = '';
    private $password    = '';
    private $domainAPI   = '';
    private $apiParams   = '';
    private $logFilePath = '';
    
    function __construct($login, $password, $domainAPI)
    {
        $this->login     = $login;
        $this->password  = $password;
        $this->domainAPI = $domainAPI;
    }
    
    public function setParams($apiParams){
        $this->apiParams = $apiParams;
    }
    
    public function getParams()
    {
        return $this->apiParams;
    }
    
    public function getResult()
    {
        $res = array();
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->domainAPI.$this->apiParams);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, "$this->login:$this->password");
            $jsonResult = curl_exec($ch);
            curl_close($ch);
            $res = json_decode($jsonResult, true);
        } catch (Exception $e){
            $this->writeLog($e->getMessage());
        }
        
        if(empty($res)){
            $this->writeLog("Error: Empty CURL Source: ".$this->domainAPI.$this->apiParams);
        } 
        
        if($res["name"] == "Unauthorized"){
            $printText = print_r($res, true);
            $this->writeLog("Error: ". $printText);
        }
        
        return $res;
    }
    
    public function writeLog($str){
        if(!empty($this->logFilePath)){
            file_put_contents($this->logFilePath, $str);
        }
    }
    
    public function setLogFile($filePath)
    {
        $this->logFilePath = $filePath;
    }
    
    public function getLogFile()
    {
        return $this->logFilePath;
    }
    
    public function getResultByParams($apiParams)
    {
        $this->setParams($apiParams);
        return $this->getResult();
    }
    
    public function getSeries()
    {
        $res = array();
        $apiParams = "/series";
        $this->setParams($apiParams);
        $res = $this->getResult();
        usort($res, 'self::compareByTitle');
        return $res;
    }
    
    public function getBrands()
    {
        $res = array();
        $apiParams = "/brands";
        $this->setParams($apiParams);
        $res = $this->getResult();
        usort($res, 'self::compareByTitle');
        return $res;
    }
    public function get�ertificates()
    {
        $res = array();
        $apiParams = "/certificates";
        $this->setParams($apiParams);
        $res = $this->getResult();
        //usort($res, 'self::compareByTitle');
        return $res;
    }
    
    public function getStocks()
    {
        $res = array();
        $apiParams = "/stocks";
        $this->setParams($apiParams);
        $res = $this->getResult();
        return $res;
    }
    
    public function getProps()
    {
        $apiParams = "/props";
        $this->setParams($apiParams);
        $res = $this->getResult();
        usort($res, 'self::compareByTitle');
        return $res;
    }
    
    public function getProduct($productID)
    {
        $apiParams = "/prods?id={$productID}";
        $this->setParams($apiParams);
        return $this->getResult();
    }
    
    public function getProductList($page=1)
    {
        $apiParams = "/products?expand=fileProduct&page={$page}";
        $this->setParams($apiParams);
        return $this->getResult();
    }
    
    private static function compareByTitle($a, $b) {
        return strcmp($a["title"], $b["title"]);
    }
}

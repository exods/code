<?php

$CONFIG_VERSION = "0.0.1_";
$CONFIG_VERSION = time();

// API PBD  
$CONFIG_API = array(
    "login"     => '************',
    "password"  => '************',
    'domainApi' => 'https://*************',
);

$IBLOCK_ID = CATALOG_IBLOCK_ID;
if ($_GET["IBLOCK_ID"]) {
    $IBLOCK_ID = $_GET["IBLOCK_ID"];
}

$limit = 32000;  // test
$CONFIG_UPLOAD_DIR = "/upload/certificates_files";
$CONFIG_APP_URL = "/local/cron/cron_certificates";

// SOURCE FILE 
$certificatesJsonFile = $_SERVER["DOCUMENT_ROOT"] . "/upload/certificates_files/json/certificates_" . date("Y.m.d") . ".json";

// LOG FILE  
$certificatesLogFile = $_SERVER["DOCUMENT_ROOT"] . "/upload/certificates_files/logs/certificates_log_" . date("Y.m.d") . "__iblock_" . $IBLOCK_ID . ".txt";

// LOG FUNCTION 
function setDataToLog($string, $logFile)
{
    file_put_contents($logFile, $string . '  ' . date("H:i:s") . PHP_EOL, FILE_APPEND);
}
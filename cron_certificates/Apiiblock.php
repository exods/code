<?php
class Apiiblock {
    
    private $iblockID = '';
    
    function __construct($iblockID = 30)
    {
        $this->iblockID     = $iblockID;
    }
    
    public function setProps($CODE, $VALUE)
    {
        
    }
    
    public function getProps($CODE)
    {
         
    }
    
    public function getProductByArticle($article)
    {
        $res = array(); 
        $arFilter = array("PROPERTY_ARTICLE"=>$article); 
        $res = $this->getElementsByFilter($arFilter); 
        
        $res = array_map(function($item){ 
            return array( 
              "NAME"              => $item["NAME"], 
              "HASH_1C"           => $item["PROPS"]["HASH_1C"]["VALUE"], // HASH - ПОТОМ УДАЛИТЬ! 
              "ARTICLE"           => $item["PROPS"]["ARTICLE"]["VALUE"], 
              "CODE"              => $item["CODE"], 
              "DETAIL_PAGE_URL"   => $item["DETAIL_PAGE_URL"], 
              "ID"                => $item["ID"],
              "IBLOCK_ID"         => $item["IBLOCK_ID"], 
              "IBLOCK_SECTION_ID" => $item["IBLOCK_SECTION_ID"], 
              "ACTIVE"            => $item["ACTIVE"],  
              "PROPS"             => $item["PROPS"], 
            ); 
        }, $res);
        
        if(!empty($res)) {
            $res = $res[0];
        }
        return $res; 
    } 
    
    public function getElementsByFilter($arFilter = array()){ 
        $arRes    = array(); 
        $arOrder  = Array("SORT"=>"ASC"); 
        $arFilter = array_merge(array('IBLOCK_ID'=>$this->iblockID, "ACTIVE"=>"Y", "INCLUDE_SUBSECTIONS"=>"Y"), $arFilter);    
        $arSelect = Array("IBLOCK_ID", "ID", 'NAME', 'SECTION_ID', 'DETAIL_PAGE_URL', 'ACTIVE');  
        $res = CIBlockElement::GetList($arOrder, $arFilter, false, array("nPageSize" => 2, "iNumPage"=>1), $arSelect);   
        
        $i = 0;
        while($ob = $res->GetNextElement()){
            $arFields  = $ob->GetFields();
            $arRes[$i] = $arFields;
            $arProps   = $ob->GetProperties();
            $arRes[$i]["PROPS"] = $arProps;
            $i++;
        }
        return $arRes;
    }
    
    public function getElementsIDbyFilter($IBLOCK_ID, $arFilter = array()){
        $arOrder  = Array("SORT"=>"ASC");
        $arFilter = array_merge(array('IBLOCK_ID'=>$IBLOCK_ID, "ACTIVE"=>"Y", "INCLUDE_SUBSECTIONS"=>"Y"), $arFilter);
        $arSelect = Array("IBLOCK_ID", "ID", 'NAME', 'SECTION_ID');
        $res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
        $arID  = array();
        while($ob = $res->GetNextElement()){  
            $arFields = $ob->GetFields(); 
            $arID[] = $arFields["ID"];  
        }
        return $arID; 
    }
    
    public function setDocumentName($IBLOCK_ID, $ELEMENT_ID, $VALUE){
        CIblockElement::SetPropertyValuesEx($ELEMENT_ID, $IBLOCK_ID, array("DOCUMENT_NAME" => $VALUE));
    }
    
    public function setDocumentURL($IBLOCK_ID, $ELEMENT_ID, $VALUE){
        CIblockElement::SetPropertyValuesEx($ELEMENT_ID, $IBLOCK_ID, array("DOCUMENTS_LINK" => $VALUE));
    }
    
    private static function compareByTitle($a, $b) {
        return strcmp($a["title"], $b["title"]);
    }
}

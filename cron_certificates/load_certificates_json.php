<?
include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

include_once("Apipbd.php"); 
include_once("Apiiblock.php"); 
include_once("func.php"); 
include_once("config.php"); 

// WRITE TO JSON
$certificatesJsonFile = $_SERVER["DOCUMENT_ROOT"]."/upload/certificates_files/json/certificates_".date("Y.m.d").".json";

// LOG FILE
$certificatesLogFile  = $_SERVER["DOCUMENT_ROOT"]."/upload/certificates_files/logs/certificates_log_".date("Y.m.d").".txt";

$obAPI = new Apipbd($CONFIG_API["login"], $CONFIG_API["password"], $CONFIG_API["domainApi"]);  
$obAPI->setLogFile($certificatesLogFile);

$certificatesSource    = $obAPI->get�ertificates();


$certificatesJsonItems = $certificatesSource["items"];

if(empty($certificatesJsonItems)){
    $logText = "Error: Empty ITEMS from API PBD!";  
    setDataToLog($logText, $certificatesLogFile);
}

$certificatesJSON = json_encode($certificatesJsonItems);
file_put_contents($certificatesJsonFile, $certificatesJSON);

pre("==== LOAD JSON =====");  
pre($certificatesJsonFile);
pre("==== SEE LOGS ======"); 
pre($certificatesLogFile);


<?
include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); 

include_once("Apipbd.php");
include_once("Apiiblock.php");
include_once("func.php");
include_once("config.php");

CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");

$iblockAPI = new Apiiblock($IBLOCK_ID);
$logText   = "";

// CHECK IF JSON FILE EXISTS
$certificatesItemsArr = array();
if(!file_exists($certificatesJsonFile)){
    $logText = "Error: JSON file not found!"; 
    setDataToLog($logText, $certificatesLogFile);
    pre($logText);  
    die();
}

// CHECK IF DATA IN JSON FILE
$certificatesFromJson = file_get_contents($certificatesJsonFile);
$certificatesItemsArr = json_decode($certificatesFromJson, true);

$TOTAL_COUNT = count($certificatesItemsArr);

if(empty($TOTAL_COUNT)){ 
    $logText = "Error: Empty total_count in API PBD"; 
    setDataToLog($logText, $certificatesLogFile);
    pre($logText); 
    die();
}

$productList = cronGetProducts($IBLOCK_ID); // Article=>ID  

pre("=========================================="); 
pre("UPDATE Certificates");
pre("=========================================="); 
pre("Certificates Source: {$certificatesJsonFile}");
pre("Certificates Log File: {$certificatesLogFile}");
pre("=========================================="); 
pre("TOTAL_COUNT = {$TOTAL_COUNT}"); 
pre("IBLOCK_ID = {$IBLOCK_ID}"); 
pre("=========================================="); 

setDataToLog("==========================================", $certificatesLogFile);
setDataToLog("Start Update Certificates!", $certificatesLogFile);
setDataToLog("==========================================", $certificatesLogFile);
setDataToLog("TOTAL_COUNT = {$TOTAL_COUNT}", $certificatesLogFile);
setDataToLog("IBLOCK_ID = {$IBLOCK_ID}", $certificatesLogFile);
setDataToLog("==========================================", $certificatesLogFile);

$i = 0;
foreach($certificatesItemsArr as $arItem){
    $storeCertificates    = $arItem["title_certificate"];
    $storeCertificatesURL = $arItem["file"];
    $productArticle       = $arItem["article_number_product"];
//    pre($storeCertificates);
//    pre($storeCertificatesURL);
//    pre($productArticle);
    $ELEMENT_ID      = $productList[$productArticle];
//    pre($ELEMENT_ID);


    if(!empty($ELEMENT_ID)){ 
         // pre("article: {$productArticle}, count: {$storeCertificates}, id: {$ELEMENT_ID}");
        try{
            $iblockAPI->setDocumentName($IBLOCK_ID, $ELEMENT_ID, $storeCertificates);
            $iblockAPI->setDocumentURL($IBLOCK_ID, $ELEMENT_ID, $storeCertificatesURL);
            setDataToLog("Success: article: {$productArticle},NAME: {$storeCertificates}, URL: {$storeCertificatesURL}, id: {$ELEMENT_ID}", $certificatesLogFile);
        }catch (Exception $e){
            setDataToLog("Error: Can't Update Element for article: {$productArticle},NAME: {$storeCertificates}, URL: {$storeCertificatesURL}, id: {$ELEMENT_ID}", $certificatesLogFile);
        }
    } else { 
        setDataToLog("Error: Not found element for article: {$productArticle} in IBLOCK_ID={$IBLOCK_ID}", $certificatesLogFile);
    }

    // QUIT 
    if($i > $limit) { break; }  
    $i++; 
} 
setDataToLog("========================", $certificatesLogFile);
setDataToLog("Finish", $certificatesLogFile);
setDataToLog("========================", $certificatesLogFile);

 //TEST
 // $storeCertificates = 'Declaration';
 // $ELEMENT_ID        = 1503537;
 // $iblockAPI->setDocumentURL($IBLOCK_ID, $ELEMENT_ID, $storeCertificates);
 // setDataToLog("Success: article: {$productArticle}, count: {$storeCertificates}, id: {$ELEMENT_ID}", $certificatesLogFile);

function cronGetProducts($IBLOCK_ID){ 
    $productsRes = array(); 
    $rsElement = CIBlockElement::GetList([], 
        ['IBLOCK_ID' => $IBLOCK_ID, 'ACTIVE' => 'Y'], 
        false, 
        false, 
        ["ID", "XML_ID", 'IBLOCK_SECTION_ID', 'PROPERTY_SERIES', 'PROPERTY_ARTICLE', 'IBLOCK_ID'] 
    ); 
    while ($arElement = $rsElement->Fetch()) { 
        $productsRes[$arElement['PROPERTY_ARTICLE_VALUE']] = $arElement["ID"];   
    } 
    return $productsRes; 
} 


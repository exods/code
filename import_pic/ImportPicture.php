<?

class ImportPicture
{

    private $dir;
    private $array_files;
    private $deleteOld = false;


    public function __construct($dir)
    {
        $this->dir = $dir;
    }


    protected function getDir()
    {
        return $this->dir;
    }


    public function getArrayFiles()
    {
        $this->setArrayFiles();
        return $this->array_files;
    }

    private function setArrayFiles()
    {
        $this->array_files = $this->readDir();
        return $this;
    }

    private function readDir()
    {
        $arrayFiles = scandir($this->dir);
        $arrayFiles = $this->transformArrayFiles($arrayFiles);

        return $arrayFiles;
    }

    public function pre($array)
    {
        echo '<div >
      <pre>';
        print_r($array);
        echo '</pre>
     </div>';
    }

    private function transformArrayFiles($array)
    {
        $group_array = [];
        foreach ($array as $element) {

            $temp_variable = mb_substr($element, 0, 8);
            if (strlen($element)>6){
                $group_array[$temp_variable][] = $element;
            }
        }
        return $group_array;

    }
    public function updateData(array $IBLOCK_ID_ARRAY){

        $arrFiles = $this->getArrayFiles();

        foreach ($IBLOCK_ID_ARRAY as $IBLOCK){

            $res = CIBlockElement::GetList(
                [],
                ['IBLOCK_ID'=>$IBLOCK,'PROPERTY_ARTICLE'=>array_keys($arrFiles),'ACTIVE'=>'Y'],
                false,
                false,
                ['IBLOCK','ID','PROPERTY_FOTOGALEREYA','PREVIEW_PICTURE','DETAIL_PICTURE','PROPERTY_ARTICLE']
            );
            while($result = $res->Fetch()) {
                $catalog_elements[$result['PROPERTY_ARTICLE_VALUE']] = $result;
            }
            $el = new CIBlockElement;

//                $this->pre($catalog_elements);

                foreach ($catalog_elements as $key => $element) {

                    $fileArray = array_map(function ($path){
                        return CFile::MakeFileArray($this->dir.'/'.$path);
                    },$arrFiles[$key]);
                    if(!$this->deleteOld){
                        CIBlockElement::SetPropertyValuesEx($element['ID'], $IBLOCK, ['FOTOGALEREYA' => $fileArray]);//Свойства(PROPERTY_CODE)
                        $result = true;
                    } else {
//                        $result = CIBlockElement::SetPropertyValueCode($element['ID'], 'FOTOGALEREYA', $fileArray);// Свойства(PROPERTY_CODE)
                    }
                   //CIBlockElement::SetPropertyValuesEx($element['ID'],$IBLOCK,["782"=> $fileArray]);

                    $res = $el->Update($element['ID'],
                        [
                            'TIMESTAMP_X' => FALSE,
                            'PREVIEW_PICTURE'  => CFile::MakeFileArray($this->dir.'/'.$arrFiles[$key][0]),
                            'DETAIL_PICTURE'  => CFile::MakeFileArray($this->dir.'/'.$arrFiles[$key][0])
                        ]
                    );

                    if($res){
                        echo 'Элемент с ID - '.$element['ID'].' успешно изменён.'.'<br>';
                    }else{
                        echo 'Ошибка изменения Элемента с ID - '.$element['ID'];
                    }

                }

            }
        }



}
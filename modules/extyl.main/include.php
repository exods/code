<?php

use Bitrix\Main\Loader;

$arClasses = array(
    'Extyl\\cMainTest' => 'classes/general/cMainTest.php',
    'Extyl\\Controller\\Ajax' => 'lib/Controller/Ajax.php',
    'CustomRestProvider' => 'classes/general/CustomRestProvider.php',
);

Loader::registerAutoLoadClasses('extyl.main', $arClasses);

<?php

namespace Extyl\Controller;

use Bitrix\Main\Engine\Controller as EngineController;

class Ajax extends EngineController implements \Bitrix\Main\Engine\Contract\Controllerable
{

    public function configureActions()
    {
        return [
            'getUserNameVol' => [
                'prefilters' => []
            ]
        ];
    }

    public function getUserNameVolAction(int $id)
    {
        $user = '';
        $ID = 1;
        $pattern = array('/(а|е|ё|и|о|у|ы|э|ю|я)/ui', '/(a|e|i|o|u|y)/ui', '/\s+/');
        $rsUser = \CUser::GetByID($ID);
        $arUser = $rsUser->Fetch();
        $user .= $arUser['NAME'];
        $user .= $arUser['LAST_NAME'];
        $user .= $arUser['SECOND_NAME'];

        return [preg_replace($pattern, '', mb_strtolower($user))];
    }
}

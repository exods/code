<?php
$MESS['EXTYL_MSPI_MODULE_NAME'] = 'Extyl. Миграции из MS Project';
$MESS['EXTYL_MSPI_MODULE_DESCRIPTION'] = 'Миграция задач и проектов из MS Project';
$MESS['EXTYL_MSPI_PARTNER_NAME'] = 'Extyl - Разработка и поддержка инфосистем: АИС, порталы, мобильные приложения.';
$MESS['EXTYL_MSPI_PARTNER_URI'] = 'https://www.extyl-pro.ru/';

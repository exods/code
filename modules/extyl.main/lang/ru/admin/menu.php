<?php
$MESS['EXTYL_MSPI_MENU_TEXT'] = 'Импорт MS Project';
$MESS['EXTYL_MSPI_MENU_TITLE'] = 'Импорт MS Project';
$MESS['EXTYL_MSPI_MENU_RESOURCES_TITLE'] = 'Генерация ресурсов';
$MESS['EXTYL_MSPI_MENU_RESOURCES_TEXT'] = 'Генерация ресурсов';
$MESS['EXTYL_MSPI_MENU_IMPORT_TITLE'] = 'Импорт задач и проектов';
$MESS['EXTYL_MSPI_MENU_IMPORT_TEXT'] = 'Импорт задач и проектов';

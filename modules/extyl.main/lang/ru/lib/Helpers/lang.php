<?php
$MESS['HLP_ERROR_COPY_REQUEST_FILE'] = 'Ошибка копирования файла заявки';
$MESS['HLP_ERROR_FOLDER_DOESNT_EXIST'] = 'Раздел не существует: #DIR#';
$MESS['HLP_ERROR_FILES_NOT_EXISTS'] = 'Исходные файлы для парсинга не найдены';

$MESS['HLP_ERROR_REQUEST_UF_PERSONAL_ID'] = 'В заявке не указан ответственный пользователь (UF_PERSONAL_ID)';
$MESS['HLP_ERROR_REQUEST_USER_NOT_FOUND'] = 'Пользователь из заявки не найден (#CODE#)';
$MESS['HLP_ERROR_COPY_RESOURCE_MAP_FILE'] = 'Ошибка копирования файла маппинга ресурсов';
$MESS['HLP_ERROR_MODULES_NOT_INSTALLED'] = 'Необходимые модули не установлены';

$MESS['HLP_ERROR_ADD_PROJECT'] = 'Ошибка создания проекта';
$MESS['HLP_ERROR_RESOURCES_NOT_FOUND'] = 'В файле #FILE_NAME# не найдены ресурсы';
$MESS['HLP_ERROR_RESOURCE_NOT_EXIST'] = 'Ресурс #NAME#  не найден среди ранее зарегистрированных. Пропущен.';
$MESS['HLP_ERROR_RESOURCE_USER_ID_DEFINED'] = 'Ресурс #NAME#: UF_PERSONAL_ID не задан, по ФИО пользователь найден (USER_ID определен)';
$MESS['HLP_ERROR_RESOURCE_SKIP'] = 'Ресурс #NAME#: UF_PERSONAL_ID не задан, по ФИО не найден - пропущен';
$MESS['HLP_ERROR_NOT_FOUND_TASK_WBS_0'] = 'Не найдена задача с WBS = 0 при типе миграции PROJECT (#FILE_NAME#)';

$MESS['HLP_RESOURCE_ADDED'] = 'Ресурс #NAME# не найден (по UF_PERSONAL_ID, ФИО), добавлен (ID = #ID#)';
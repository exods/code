<?php

$MESS['RW_STEP_start'] = 'Регистрация запроса. Инициализация.';
$MESS['RW_STEP_listFiles'] = 'Получение списка файлов';
$MESS['RW_STEP_parseFile'] = 'Парсинг файла #FILE_NAME#';
$MESS['RW_STEP_searchUsers'] = 'Поиск и сопоставление пользователей';
$MESS['RW_STEP_generateResourceFile'] = 'Генерация файла ресурсов';
$MESS['RW_STEP_end'] = 'Окончание';

$MESS['RW_FILES_NOT_EXISTS'] = 'Файлы для парсинга не найдены';
$MESS['RW_RESOURCES_NOT_FOUND'] = 'В файле #FILE_NAME# не найдены ресурсы';
$MESS['RW_PARSE_FILES_END'] = 'Конец парсинга файлов';

$MESS['RW_HEADER_ID'] = 'ID ресурса';
$MESS['RW_HEADER_FILE_NAME'] = 'Имя файла';
$MESS['RW_HEADER_NAME'] = 'Название ресурса';
$MESS['RW_HEADER_ORIGINAL_NAME'] = 'Название ресурса';
$MESS['RW_HEADER_USER_ID'] = 'ID Битрикс';
$MESS['RW_HEADER_USER_XML_ID'] = 'XML_ID';
$MESS['RW_HEADER_UF_PERSONAL_ID'] = 'UF_PERSONAL_ID';

$MESS['RW_ERROR_FILE_CREATE'] = 'Не удалось создать файл';
$MESS['RW_ERROR_CREATING_SRC_FOLDER'] = 'Ошибка открытия директории по пути: #PATH#';
$MESS['RW_ERROR_CREATING_DEST_FOLDER'] = 'Ошибка создания директориии назначения: #PATH#';


$MESS['IW_STEP_start'] = 'Регистрация запроса. Инициализация.';
$MESS['IW_STEP_parseRequestFile'] = 'Загрузка файла с заявкой, парсинг, сохранение';
$MESS['IW_STEP_listFiles'] = 'Поиск файлов согласно заявке';
$MESS['IW_STEP_importSourceFileResources'] = 'Составление ресурсов (#FILE_NAME#)';
$MESS['IW_STEP_searchUsers'] = 'Поиск и сопоставление пользователей';
$MESS['IW_STEP_parseResourceMapFile'] = 'Парсинг файла маппинга с ресурсами (). Сохранение в БД';
$MESS['IW_STEP_importFromFilesToDb'] = 'Импорт задач и проектов в собств. таблицы';
$MESS['IW_STEP_importTasksByRequests'] = 'Импорт из файла #FILE_NAME#';
$MESS['IW_STEP_end'] = 'Окончание';






$MESS['IW_ERROR_CREATING_SRC_FOLDER'] = 'Ошибка открытия директории по пути: #PATH#';
$MESS['IW_ERROR_CREATING_DEST_FOLDER'] = 'Ошибка создания директориии назначения: #PATH#';



$MESS['IW_REQUEST_STATUS_INIT'] = 'Не обработан';
$MESS['IW_REQUEST_STATUS_PROCESS'] = 'В процессе';
$MESS['IW_REQUEST_STATUS_ERROR'] = 'Ошибка';
$MESS['IW_REQUEST_STATUS_ABORTED'] = 'Прерван';
$MESS['IW_REQUEST_STATUS_COMPLETED'] = 'Завершено';
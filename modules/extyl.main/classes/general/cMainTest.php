<?php

namespace Extyl;

class cMainTest
{
    static $MODULE_ID = "extyl.test";

    /**
     * Получение гласных букв
     * @param $arFields
     * @return bool
     */

    static function getUserNameVol(int $ID): string
    {       $pattern = array('/(а|е|ё|и|о|у|ы|э|ю|я)/ui', '/(a|e|i|o|u|y)/ui', '/\s+/');
            $rsUser = \CUser::GetByID($ID);
            $arUser = $rsUser->Fetch();
            $user .= $arUser['NAME'];
            $user .= $arUser['LAST_NAME'];
            $user .= $arUser['SECOND_NAME'];

        return preg_replace($pattern, '', mb_strtolower($user));
    }
    public static function OnRestServiceBuildDescription()
    {
        return array(
            'custom' => array(
                'get.username.vol' => array('callback' =>
                    array(__CLASS__, 'getUserNameVol'),'options' =>
                    array()),
            )
        );
    }
}
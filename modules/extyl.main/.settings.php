<?php

return [
    'controllers' => [
        'value' => [
            'namespaces' => [
                '\\Extyl\\Controller' => 'api',
                'restIntegration' => [
                    'enabled' => true,
                ],
            ],
        ],
        'readonly' => true,
    ]
];

<?php

namespace App\Utils;

use CIBlockSection;
use CUser;

class ExportUsers
{
    /**
     * @var array
     */
    private array $users = [];
    private array $selectFields = [
        'ID',
        'LOGIN',
        'NAME',
        'SECOND_NAME',
        'LAST_NAME',
        'EMAIL',
        'PERSONAL_PHOTO',
        'UF_COMPANY',
        'UF_PERSONAL_ID',

    ];

    /**
     * @return array
     */
    public function getSelectFields(): array
    {
        return $this->selectFields;
    }

    /**
     * @param array $users
     */
    private function setUsers(array $users): void
    {
        $this->users = $users;
    }

    /**
     * @return string
     */
    private function getSectionName($ID)
    {
        if ($ID) {
            $res = CIBlockSection::GetByID($ID);
            if ($ar_res = $res->GetNext()) {
                return $ar_res['NAME'];

            }
        }

    }

    /**
     * @return void
     */
    private function readUsers(): void
    {
        $usersData = [];
        $filter = ['ACTIVE' => 'Y'];
        $rsUsers = CUser::GetList(($by = 'id'), ($order = 'desc'), $filter, ["FIELDS" => $this->selectFields, "SELECT" => ['UF_COMPANY', 'UF_PERSONAL_ID']]);
        while ($arUser = $rsUsers->Fetch()) {
            $arUser['UF_COMPANY'] = $this->getSectionName($arUser['UF_COMPANY']);
            if ($arUser['PERSONAL_PHOTO'] === null) {
                $usersData[] = $arUser;
            }
        }
        $this->setUsers($usersData);
    }

    /**
     * @return array
     */
    public function getUsers(): array
    {
        return $this->users;
    }

    public function init(): array
    {
        $this->readUsers();
        return $this->getUsers();
    }


}
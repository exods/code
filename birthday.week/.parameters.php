<?php
if (!define('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

CModule::IncludeModule('intranet');

$arUserFieldNames = [
    'PERSONAL_PHOTO',
    'FULL_NAME','ID',
    'LOGIN',
    'NAME',
    'SECOND_NAME',
    'LAST_NAME',
    'EMAIL',
    'DATE_REGISTER',
    'PERSONAL_PROFESSION',
    'PERSONAL_WWW',
    'PERSONAL_BIRTHDAY',
    'PERSONAL_ICQ',
    'PERSONAL_GENDER',
    'PERSONAL_PHONE',
    'PERSONAL_FAX',
    'PERSONAL_PAGE',
    'PERSONAL_MOBILE',
    'PERSONAL_STREET',
    'PERSONAL_MAILBOX',
    'PERSONAL_CITY',
    'PERSONAL_ZIP',
    'PERSONAL_COUNTRY',
    'PERSONAL_NOTES',
    'PERSONAL_POSITION',
    'WORK_POSITION',
    'WORK_COMPANY',
    'WORK_PHONE',
    'WORK_FAX',
    'ADMIN_NOTES',
    'XML_ID'
];

$userProp = [];
foreach ($arUserFieldNames as $name) {
    $userProp[$name] = GetMessage('ISL_' . $name);
}

$arRes = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('USER', 0, LANGUAGE_ID);
if(!empty($arRes)) {
    foreach ($arRes as $key => $val){
        $userProp[$val['FIELD_NAME']] = '* ' . (strlen($val['EDIT_FORM_LABEL']) > 0 ? $val['EDIT_FORM_LABEL'] : $val['FIELD_NAME']);
    }
}

$arComponentParameters = [
    'GROUPS' => [],
    'PARAMETERS' => [
        'NUM_USERS' => [
            'TYPE' => 'STRING',
            'MULTIPLE' => 'N',
            'DEFAULT' => 10,
            'NAME' => GetMessage('INTR_ISBN_PARAM_NUM_USERS'),
            'PARENT' => 'BASE',
        ],
        'USER_PROPERTY' => [
            'NAME' => GetMessage('INTR_ISBN_PARAM_USER_PROPERTY'),
            'TYPE' => 'LIST',
            'VALUES' => $userProp,
            'MULTIPLE' => 'Y',
            'DEFAULT' => ['UF_DEPARTMENT', 'PERSONAL_PHONE', 'PERSONAL_MOBILE', 'WORK_PHONE'],
        ],
        'CACHE_TIME' => ['DEFAULT' => 86400],
    ],
];

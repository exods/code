<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */

// Шапка
$arWeek = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
$arWeekEn = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
$this->SetViewTarget("sidebar2", 200);
?>

<div class="custom-widget-birthday js-widget-birthday">
    <div class="custom-widget-birthday__title">
        <a href="/company/birthdays.php"><?=GetMessage('BIRTHDAYS')?></a>
        <a href="/company/birthdays.php">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M21 10H3M16 2V6M8 2V6M7.8 22H16.2C17.8802 22 18.7202 22 19.362 21.673C19.9265 21.3854 20.3854 20.9265 20.673 20.362C21 19.7202 21 18.8802 21 17.2V8.8C21 7.11984 21 6.27976 20.673 5.63803C20.3854 5.07354 19.9265 4.6146 19.362 4.32698C18.7202 4 17.8802 4 16.2 4H7.8C6.11984 4 5.27976 4 4.63803 4.32698C4.07354 4.6146 3.6146 5.07354 3.32698 5.63803C3 6.27976 3 7.11984 3 8.8V17.2C3 18.8802 3 19.7202 3.32698 20.362C3.6146 20.9265 4.07354 21.3854 4.63803 21.673C5.27976 22 6.11984 22 7.8 22Z" stroke="#767E89" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
        </a>
    </div>
    <div class="custom-widget-birthday__week-days">
        <?for ($i = 1; $i <= 7; $i++) {?>
            <div class="custom-widget-birthday__week-day" data-id="<?=$i - 1?>">
                <?= $arWeek[$i -1]?>
            </div>
        <?}?>
    </div>
    <div class="custom-widget-birthday__body">

        <?for ($i = 1; $i <= 7; $i++) {?>
            <div class="custom-widget-birthday__tab" data-id="<?=$i - 1?>">
                <? if(isset($arResult[$arWeekEn[$i -1]])) { ?>
                <?
                    foreach ($arResult[$arWeekEn[$i -1]] as $nTimestamp => $arData) {
                        $sDateFormat = FormatDate('j F o', $nTimestamp);
                ?>
                        <div class="custom-widget-birthday__date">
                                <?=$sDateFormat?>
                        </div>
                        <div class="custom-widget-birthday__birthdays">
                            <?foreach ($arData as $arUser) {?>
                                <div class="custom-widget-birthday__employee">
                                    <div class="custom-widget-birthday__employee-background">
                                        <a href="<?=$arUser['DETAIL_URL']?>">
                                            <? if (!empty($arUser['PERSONAL_PHOTO']) && is_array($arUser['PERSONAL_PHOTO'])) { ?>
                                                <img src="<?= $arUser['PERSONAL_PHOTO']['src'] ?>"
                                                     alt="<?= ($arUser['LAST_NAME'] . ' ' . $arUser['NAME']) ?>">
                                            <? } ?>
                                        </a>
                                    </div>
                                    <div class="custom-widget-birthday__employee-info">
                                        <a href="<?=$arUser['DETAIL_URL']?>" class="custom-widget-birthday__employee-info-name"><?=$arUser['LAST_NAME'] . ' ' . $arUser['NAME']?></a>
                                        <p class="custom-widget-birthday__employee-info-grade"><?=$arUser['WORK_POSITION']?></p>
                                        <a href="<?=$arUser['UF_DEPARTMENT_URL']?>" class="custom-widget-birthday__employee-department"><?=$arUser['UF_DEPARTMENT']?></a>
                                    </div>
                                </div>
                            <?}?>
                        </div>
                    <?}?>
                <? } else { ?>
                    <div class="custom-widget-birthday__not-found">
                        <?=GetMessage('BIRTHDAYS_NOT_FOUND')?>
                    </div>
                <? } ?>
            </div>
        <?}?>
    </div>
</div>

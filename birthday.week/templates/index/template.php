<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/** @var array $arParams */
/** @var array $arResult */

// Шапка
$arWeek = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
$arWeekEn = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
$this->SetViewTarget("sidebar2", 200);
?>

<div class="custom-widget-birthday js-widget-birthday index">
    <div class="custom-widget-birthday__title">
        <a href="/company/birthdays.php"><?=GetMessage('BIRTHDAYS')?></a>
        <a href="/company/birthdays.php">
            <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#90bc56">
                    <path d="M0 0h24v24H0V0z" fill="none"/><path d="M20 3h-1V1h-2v2H7V1H5v2H4c-1.1 0-2 .9-2 2v16c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 18H4V10h16v11zm0-13H4V5h16v3z"/>
            </svg>
        </a>
    </div>
    <div class="custom-widget-birthday__week-days">
        <?for ($i = 1; $i <= 7; $i++) {?>
            <div class="custom-widget-birthday__week-day" data-id="<?=$i - 1?>">
                <?= $arWeek[$i -1]?>
            </div>
        <?}?>
    </div>
    <div class="custom-widget-birthday__body">

        <?for ($i = 1; $i <= 7; $i++) {?>
            <div class="custom-widget-birthday__tab" data-id="<?=$i - 1?>">
                <? if(isset($arResult[$arWeekEn[$i -1]])) { ?>
                <?
                    foreach ($arResult[$arWeekEn[$i -1]] as $nTimestamp => $arData) {
                        $sDateFormat = FormatDate('j F o', $nTimestamp);
                ?>
                        <div class="custom-widget-birthday__date">
                                <?=$sDateFormat?>
                        </div>
                        <div class="custom-widget-birthday__birthdays">
                            <?foreach ($arData as $arUser) {?>
                                <div class="custom-widget-birthday__employee">
                                    <div class="custom-widget-birthday__employee-background">
                                        <a href="<?=$arUser['DETAIL_URL']?>">
                                            <? if (!empty($arUser['PERSONAL_PHOTO']) && is_array($arUser['PERSONAL_PHOTO'])) { ?>
                                                <img src="<?= $arUser['PERSONAL_PHOTO']['src'] ?>"
                                                     alt="<?= ($arUser['LAST_NAME'] . ' ' . $arUser['NAME']) ?>">
                                            <? } ?>
                                        </a>
                                    </div>
                                    <div class="custom-widget-birthday__employee-info">
                                        <a href="<?=$arUser['DETAIL_URL']?>" class="custom-widget-birthday__employee-info-name"><?=$arUser['LAST_NAME'] . ' ' . $arUser['NAME']?></a>
                                        <p class="custom-widget-birthday__employee-info-grade"><?=$arUser['WORK_POSITION']?></p>
                                        <a href="<?=$arUser['UF_DEPARTMENT_URL']?>" class="custom-widget-birthday__employee-department"><?=$arUser['UF_DEPARTMENT']?></a>
                                    </div>
                                </div>
                            <?}?>
                        </div>
                    <?}?>
                <? } else { ?>
                    <div class="custom-widget-birthday__not-found">
                        <?=GetMessage('BIRTHDAYS_NOT_FOUND')?>
                    </div>
                <? } ?>
            </div>
        <?}?>
    </div>
</div>

<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\UserTable;
use Bitrix\Main\Data\Cache;
use App\Traits\FzpdHelper;

/** @var array $arParams */
/** @var array $arResult */

if (!isset($arParams['CACHE_TIME'])) {
    $arParams['CACHE_TIME'] = 86400;
}
global $CACHE_MANAGER;

// Получаем номер недели в текущем году года
$nWeekNumber = strval(date('W'));
$cache_id = md5(serialize($arParams));
$cache_dir = "/tagged_week";
// Получаем из кэша
$obCache = new CPHPCache();
if ($obCache->initCache($arParams['CACHE_TIME'], $cache_id, $cache_dir)) {
    $arResult = $obCache->getVars();
} elseif ($obCache->startDataCache()) {
    // получаем массив доступных дат
    $arAvailableDates = [];
    for ($i = 1; $i <= 7; $i++) {
        $sDateShort = FormatDate('m-d', (time() + ($i - (int)date('N')) * 86400));
        $arAvailableDates[$sDateShort] = time() + ($i - (int)date('N')) * 86400;
    }

    // Получаем информацию о структурных подразделениях (разделы в ИБ "Подразделения")
    $arDepartments = [];
    $rsSections = CIBlockSection::GetList(
        ['SORT' => 'ASC'],
        [
            'IBLOCK_ID' => FzpdHelper::getIblockId('departments'),
            'ACTIVE' => 'Y',
            'GLOBAL_ACTIVE' => 'Y',
        ],
        false,
        ['ID', 'NAME']
    );
    while ($arSection = $rsSections->Fetch()) {
        $arDepartments[$arSection['ID']] = $arSection['NAME'];
    }

    $CACHE_MANAGER->StartTagCache($cache_dir);
    $CACHE_MANAGER->RegisterTag('tagged_week');
    // Получаем пользователей
    $arUsers = [];

    $rsUser = UserTable::getList([
        'order' => ['LAST_NAME' => 'ASC'],
        'filter' => [
            'ACTIVE' => 'Y',
            '!NAME' => false,
            '!PERSONAL_BIRTHDAY' => false,
        ],
        'select' => $arParams['USER_PROPERTIES']
    ]);

    while ($arUser = $rsUser->fetch()) {
        // Получаем дату рождения
        $sDateShort = $arUser['PERSONAL_BIRTHDAY']->format('m-d');

        //Проверяем дату
        if (array_key_exists($sDateShort, $arAvailableDates)) {
            $arUser['UF_DEPARTMENT_URL'] = '/company/structure.php?set_filter_structure=Y&structure_UF_DEPARTMENT=' . $arUser['UF_DEPARTMENT'][0];
            $arUser['DETAIL_URL'] = '/company/personal/user/' . $arUser['ID'] . '/';
            $arUser['UF_DEPARTMENT'] = $arDepartments[$arUser['UF_DEPARTMENT'][0]] ?: '';
            $arUser['PERSONAL_PHOTO'] = CFile::ResizeImageGet(
                $arUser['PERSONAL_PHOTO'],
                ['width' => 100, 'height' => 100],
                'BX_RESIZE_IMAGE_PROPORTIONAL'
            );

            $weekDay = date('l', $arAvailableDates[$sDateShort]);
            $arResult[$weekDay][$arAvailableDates[$sDateShort]][$arUser['ID']] = $arUser;
        }
    }

    // Сортируем массив по дням недели
    ksort($arResult);

    $CACHE_MANAGER->EndTagCache();
    $obCache->endDataCache($arResult);
}

// Подключаем шаблон компонента
$this->IncludeComponentTemplate();